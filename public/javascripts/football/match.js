var matchSummary = [];
var h2hContent = [];
var standingsData = null;
var lineupsData = null;
var statisticsData = null;
$(document).ready(function () {

    getMatchContent();
    $(document).on("click",".game_type",function() {
        $('.game_type').removeClass('active_game_type');
        $(this).addClass('active_game_type');
        var type =  $(this).attr('data-type');
        if(type === '1'){
            getMatchContent();
        }
        if(type === '2'){
            getMatchH2HData();
        }
        if(type === '3'){
             callStanding();
        }
        if(type === '4'){
            getLineups();
        }
        if(type === '5'){
            getStatistics();
        }
    })
})

function getStatistics() {
    setStatistics(statisticsData,5);
    setPageMainContent(matchSummary, 5)
}
function setStatistics(data) {
    console.log('statisticsData',data)
    var content = "";

    if(typeof data !== "undefined" || data.length > 0){
        if(typeof data[0].possessiontime !== "undefined" && data[0].possessiontime && data[0].possessiontime !== null)
            content+=setStatsRow(data[0].possessiontime+"%","Ball Possession",data[1].possessiontime+"%");
        if(data[0].goal_attempts && data[0].goal_attempts !== null)
            content+=setStatsRow(data[0].goal_attempts,"Goal Attempts",data[1].goal_attempts);
        if(data[0].goal_attempts && data[0].goal_attempts !== null)
            content+=setStatsRow(data[0].goal_attempts,"Goal Attempts",data[1].goal_attempts);


        if(data[0].shots  && data[0].shots !== null){
            if(data[0].shots.ongoal){
                content+=setStatsRow(data[0].shots.ongoal,"Shots on Goal",data[1].shots.ongoal);
            }
            if(data[0].shots.offgoal){
                content+=setStatsRow(data[0].shots.offgoal,"Shots off Goal",data[1].shots.offgoal);
            }
            if(data[0].shots.blocked){
                content+=setStatsRow(data[0].shots.blocked,"Blocked Shots",data[1].shots.blocked);
            }
        }

        if(data[0].free_kick && data[0].free_kick !== null)
            content+=setStatsRow(data[0].free_kick,"Free Kicks",data[1].free_kick);
        if(data[0].corners && data[0].corners !== null)
            content+=setStatsRow(data[0].corners,"Corner Kicks",data[1].corners);
        if(data[0].offsides && data[0].offsides !== null)
            content+=setStatsRow(data[0].offsides,"Offsides",data[1].offsides);
        if(data[0].saves && data[0].saves !== null)
            content+=setStatsRow(data[0].saves,"Goalkeeper Saves",data[1].saves);
        if(data[0].fouls && data[0].fouls !== null)
            content+=setStatsRow(data[0].fouls,"Fouls",data[1].fouls);
        if(data[0].yellowcards && data[0].yellowcards !== null)
            content+=setStatsRow(data[0].yellowcards,"Yellow Cards",data[1].yellowcards);
        if(data[0].yellowcards && data[0].yellowredcards !== null)
            content+=setStatsRow(data[0].yellowredcards,"Red Cards",data[1].yellowredcards);
        if(data[0].passes && data[0].passes !== null)
            content+=setStatsRow(data[0].passes.total,"Total Passes",data[1].passes.total);

        if(data[0].attacks){
            if(data[0].attacks.attacks){
                content+=setStatsRow(data[0].attacks.attacks,"Attacks",data[1].attacks.attacks);
            }
            if(data[0].attacks.dangerous_attacks){
                content+=setStatsRow(data[0].attacks.dangerous_attacks,"Dangerous Attacks",data[1].attacks.dangerous_attacks);
            }
        }
    }else{
        content = "<div>No Information about Statistics</div>";
    }
    $('.bage_by_half_main').html(content)
}
function setStatsRow(t_1_info,name,t_2_info){
    return "" +
        "<div class='stats_row'>" +
        "   <div class='stats_row_in'>"+t_1_info+"</div>" +
        "   <div class='stats_row_name'>"+name+"</div>" +
        "   <div class='stats_row_in'>"+t_2_info+"</div>" +
        "</div>"
}
function getLineups() {
    setLineups(lineupsData,4);
    setPageMainContent(matchSummary, 4)
}
function setLineups(data) {
    // console.log('data',data)
    var content = "";
    if(data.length > 0){
        var team_1_id = matchSummary.localteam_id;
        var team_2_id = matchSummary.visitorteam_id;

        var teamsArr = [
            {
                name:matchSummary.localTeam.data.name,
                id:matchSummary.localTeam.data.id,
                coach:{
                    name : matchSummary.localCoach.data.fullname,
                    country : matchSummary.localCoach.data.nationality
                },
                lineup : []
            },
            {
                name:matchSummary.visitorTeam.data.name,
                id:matchSummary.visitorTeam.data.id,
                coach:{
                    name : matchSummary.visitorCoach.data.fullname,
                    country : matchSummary.visitorCoach.data.nationality
                },
                lineup : []
            }
        ]
        for(var i = 0; i < data.length; i++){
            if(data[i].team_id === teamsArr[0].id){
                teamsArr[0].lineup.push(data[i]);
            }
            if(data[i].team_id === teamsArr[1].id){
                teamsArr[1].lineup.push(data[i]);
            }
        }
        var lineContent = "";
        for(var j = 0; j < teamsArr.length; j++){
            lineContent +="" +
                "<div>" +
                "   <a href='/football/team/"+ teamsArr[j].id +"' class=' li_team_name'>"+ teamsArr[j].name +"</a>" +
                setTeamLines(teamsArr[j].lineup) +
                "<div class='team_squad_row fl_1 fl_row coach_name'>Coach - "+ teamsArr[j].coach.name +" ( "+teamsArr[j].coach.country+" )</div>" +
                "</div>"
        }
        content+="" +
            "<div class='match_info'>" +
            "   <div class='match_info_title'>Starting Lineups</div>"+
            lineContent+
            "</div>";

        $('.bage_by_half_main').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");

    }else{
        content = "<div>No Information about lineups</div>";
    }
    $('.bage_by_half_main').html(content)
}

function setTeamLines(lineup){
    var result = "";

    for(var i = 0; i < lineup.length; i++){
        var captain = "";
        if(lineup[i].captain){
            captain = "(C)"
        }
        result+= ""+
            "<div class='team_squad_row fl_1 fl_row'>" +
            "   <div class='team_squad_num'>"+lineup[i].number+"</div>" +
            "   <img class='player_country_img' src='"+ lineup[i].player.data.country.data.image_path +"' alt='"+ lineup[i].player.data.country.data.name +"'/>" +
            "   <a href='/football/player/"+lineup[i].player.data.player_id+"' class='team_squad_name fl_1 fl_row' title='"+lineup[i].player.data.display_name+"'>"+lineup[i].player.data.fullname +" "+ captain+"</a>" +
            "</div>";
    }

    return result;
}


function getMatchH2HData() {

    $('.bage_by_half_main').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(h2hContent.length !== 0){
        setMatchH2HContent(h2hContent)
    }else {
        var localTeamId = matchSummary.localTeam.data.id;
        var visitorTeamId = matchSummary.visitorTeam.data.id;
        var url = '/football/match/get-h2h';
        requestPost(url, 'localTeamId=' + localTeamId + '&visitorTeamId=' + visitorTeamId, function () {
            if (this.readyState == 4) {
                var result = JSON.parse(this.responseText);
                console.log('result',result)
                if (!result.error) {
                    h2hContent = result.data;
                    setMatchH2HContent(h2hContent)
                    setPageMainContent(matchSummary, 2)
                } else {
                    //console.log('error')
                }
            }
        })
    }
}
function callStanding(){
    $('.bage_by_half_main').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(standingsData === null){
        var url = '/football/league/standing';
        var leagueId = matchSummary.league_id;
        requestPost(url,'league_id='+leagueId,function () {
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    console.log('result.data',result.data)
                    standingsData = result.data.length > 0 ? result.data[0] : [];
                    setStandingsPopupContent(standingsData);
                }else{
                    setStandingsPopupContent([]);
                }
            }
        })
    }else {
        setStandingsPopupContent(standingsData);
    }
}

function setStandingsPopupContent(data) {
    var content = [];
    if(data.length === 0){
        content.push("<div class='no_match_text'>"+languages[siteLang].no_results+"</div>");

    }else {

        var list = data.standings.data;
        content.push("" +
            "<div class=\"standings_title fl_1 fl_row\">\n" +
            "                            <div class=\"standings_t_num\">#</div>\n" +
            "                            <div class=\"standings_t_name fl_1 fl_row\">"+languages[siteLang].standing_team+"</div>\n" +
            "                            <div class=\"standings_t_mp\">MP</div>\n" +
            "                            <div class=\"standings_t_w\">W</div>\n" +
            "                            <div class=\"standings_t_d\">D</div>\n" +
            "                            <div class=\"standings_t_l\">L</div>\n" +
            "                            <div class=\"standings_t_g\">G</div>\n" +
            "                            <div class=\"standings_t_pts\">"+languages[siteLang].standing_pts+"</div>\n" +
            "                            <div class=\"standings_t_form\">"+languages[siteLang].standing_form+"</div>\n" +
            "                        </div>");
        for (var i = 0; i < list.length; i++) {
            var positionColorClass = "";
            if (list[i].position === 1 || list[i].position === 2 || list[i].position === 3 || list[i].position === 4) {
                positionColorClass = "num_color_blue";
            } else if (list[i].position === 5 || list[i].position === 6) {
                positionColorClass = "num_color_darkred";
            } else if (list[i].position === 7) {
                positionColorClass = "num_color_pink";
            } else if (list[i].position === 18 || list[i].position === 19 || list[i].position === 20) {
                positionColorClass = "num_color_red";
            }
            let gameResCont = "";
            let game_results = list[i].recent_form.split("");
            for (let j = 0; j < game_results.length; j++) {
                let colorClass = "";
                if (game_results[j] === "W") {
                    colorClass = "item_win";
                } else if (game_results[j] === "D") {
                    colorClass = "item_no_one";
                } else {
                    colorClass = "item_loose";
                }
                gameResCont += "<div class='standings_form_item " + colorClass + "'>" + game_results[j] + "</div>";
            }

            content.push(
                " <div class='standings_row fl_1 fl_row'>" +
                "    <div class='standings_num " + positionColorClass + "' title='" + list[i].result + "'>" + list[i].position + ".</div>" +
                "    <a target='_blank' href='/football/team/" + list[i].team_id + "' class='standings_name fl_1 fl_row'>" +
                list[i].team_name +
                "    </a>" +
                "    <div class='standings_mp'>" + list[i].overall.games_played + "</div>" +
                "    <div class='standings_w'>" + list[i].overall.won + "</div>" +
                "    <div class='standings_d'>" + list[i].overall.draw + "</div>" +
                "    <div class='standings_l'>" + list[i].overall.lost + "</div>" +
                "    <div class='standings_g'>" + list[i].overall.goals_scored + " : " + list[i].overall.goals_against + "</div>" +
                "    <div class='standings_pts'>" + list[i].points + "</div>" +
                "    <div class='standings_form fl_row'>" +
                gameResCont +
                "     </div>" +
                " </div>"
            )
        }


        content.push("<div class=\"promotion_colors fl_1 fl_colum\">\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_blue\"></div> Promotion - Champions League (Group Stage)\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_darkred\"></div> Promotion - Europa League (Group Stage)\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_pink\"></div> Promotion - Europa League (Qualification)\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_red\"></div> Relegation - LaLiga2\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "                <div class=\"end_page_text\">\n" +
            "                    If teams finish on equal points at the end of the season, head-to-head matches will be the tie-breaker.\n" +
            "                </div>");


    }
    $('.bage_by_half_main').html(content)
}

function getMatchContent() {
    $('.bage_by_half_main').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(matchSummary.length !== 0){
        setmatchSummaryContent(matchSummary)
    }else {
        var matchId = $('#match_id').val();
        var url = '/football/match/get-match'
        requestPost(url, 'matchId=' + matchId + '', function () {
            // if(this.)
            if (this.readyState == 4) {
                var result = JSON.parse(this.responseText);
                if (!result.error) {
                    console.log('result.data',result.data)
                    matchSummary = result.data;
                    lineupsData = typeof result.data.lineup == "undefined" ? [] : result.data.lineup.data;
                    statisticsData =typeof result.data.stats == "undefined" ? [] : result.data.stats.data;
                    setmatchSummaryContent(matchSummary)
                    setPageMainContent(matchSummary, 1)
                } else {
                    //console.log('error')
                }
            }
        })
    }
}

function setMatchH2HContent(data,active_page) {
   console.log('setMatchH2HContent',data)
    var content = "";
    var activeMatch,activeH2h,activeStandings,activeLineups;
    if(active_page == '1'){
        activeMatch = 'active_game_type'
    }else if(active_page == '2'){
        activeH2h = 'active_game_type'
    }else if(active_page == '3'){
        activeStandings = 'active_game_type'
    }else {
        activeLineups = 'active_game_type'
    }

    $('.bage_by_half_main').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");

    if(data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            var htContent = "";
            var scoreContent = '';
            var ht_hash = [];
            var liveClass = "";
            if (
                data[i].time.status == "SUSP" ||
                data[i].time.status == "POSTP" ||
                data[i].time.status == "DELAYED" ||
                data[i].time.status == "TBA" ||
                data[i].time.status == "CANCL"
            ) {
                if (data[i].scores.ht_score !== null && data[i].scores.ht_score !== "") {
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>" + ht_hash[0] + "</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>" + ht_hash[2] + "</div>"
                } else {
                    htContent += "<div>" + data[i].time.status + "</div>";
                }
            } else if (data[i].time.status == "AWARDED") {
                htContent += "<div class='round_game_1_team_result'>" + data[i].time.status + "</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>" + data[i].scores.localteam_score + "</div>" +
                    "<div class='round_game_1_team_result'>-</div>" +
                    "<div class='round_game_1_team_result'>" + data[i].scores.visitorteam_score + "</div>";
            } else if (data[i].time.status == "HT") {
                liveClass = "live_match_game_res";
                if (data[i].scores.ht_score !== null && data[i].scores.ht_score !== "") {
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result " + liveClass + "'>" + ht_hash[0] + "</div>" +
                        "    <div class='round_game_1_team_result  " + liveClass + "'>-</div>" +
                        "    <div class='round_game_1_team_result  " + liveClass + "'>" + ht_hash[2] + "</div>"
                } else {
                    htContent += "<div class='live_match_game_res'>" + data[i].time.minute + "'</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result xx " + liveClass + "'>" + data[i].scores.localteam_score + "</div>" +
                    "<div class='round_game_1_team_result xx " + liveClass + "'>-</div>" +
                    "<div class='round_game_1_team_result xx " + liveClass + "'>" + data[i].scores.visitorteam_score + "</div>";
            } else if (data[i].time.status == "NS") {
                htContent += "<div class=''>" + data[i].time.status + "</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>-</div>";
            } else if (data[i].time.status == "LIVE") {
                liveClass = "live_match_game_res";
                htContent += "<div class='live_match_game_res'>" + data[i].time.status + " " + data[i].time.minute + "'</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result  " + liveClass + "'>" + data[i].scores.localteam_score + "</div>" +
                    "<div class='round_game_1_team_result  " + liveClass + "'>-</div>" +
                    "<div class='round_game_1_team_result  " + liveClass + "'>" + data[i].scores.visitorteam_score + "</div>";
            } else if (data[i].time.status == "FT") {
                if (data[i].scores.ht_score !== null && data[i].scores.ht_score !== "") {
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>" + ht_hash[0] + "</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>" + ht_hash[2] + "</div>"
                } else {
                    htContent += "<div>" + data[i].time.status + "</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result  '>" + data[i].scores.localteam_score + "</div>" +
                    "<div class='round_game_1_team_result  '>-</div>" +
                    "<div class='round_game_1_team_result  '>" + data[i].scores.visitorteam_score + "</div>";

            }


            // htContent += "<div class='round_game_1_team_result'>"+data[i].time.status+"</div>";
            content += "   <a href='/match/" + data[i].id + "' class='result_content  round_row fl_1 fl_row'  target='popup'" +
                " onclick=\"window.open('/football/match/" + data[i].id + "','popup','width=660,height=620'); return false;\">" +
                // roundContent += "             <div class=' round_row fl_1 fl_row'>" +
                "                            <div class='round_game_data'>" + data[i].time.starting_at.date_time + "</div>" +
                "                            <div class='round_game_teams fl_row'>" +
                // "                                    <a href='/football/team/"+data[i].localTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].localTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row_reverse'>" + data[i].localTeam.data.name + "</div>" +
                scoreContent +
                // "                                    <a href='/football/team/"+data[i].visitorTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row'>"+data[i].visitorTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row'>" + data[i].visitorTeam.data.name + "</div>" +
                "                            </div>" +
                "<div class='round_game_ht fl_row'>" +
                htContent +
                "</div>" +
                "                        </a>"
        }
    }else{
        content+= "<div class='no_match_text'>There are no results</div>"
    }
    $('.bage_by_half_main').html(content);
}

function setPageMainContent(data,active_page) {
    var activeMatch,activeH2h,activeStandings,activeLineups,activeStatistics;
    if(active_page == '1'){
        activeMatch = 'active_game_type'
    }else if(active_page == '2'){
        activeH2h = 'active_game_type'
    }else if(active_page == '3') {
        activeStandings = 'active_game_type'
    }else if(active_page == '4'){
        activeLineups = 'active_game_type'
    }else {
        activeStatistics = 'active_game_type'
    }
    var content  = "";
    var score = '';
    var status = '';
    var round = '';
    var classLive = '';
    if(data.time.status === "FT"){
        score = data.scores.localteam_score +" - "+data.scores.visitorteam_score;
        status = 'Finished'
    }else if (data.time.status === "POSTP"){
        score = '';
        status = 'Postponed'
    }else if (data.time.status === "NS"){
        score = '';
        status = 'Not Started'
    }else if (data.time.status === "CANCL"){
        score = '';
        status = 'Canceled'
    }else if (data.time.status === "AWARDED"){
        score = '';
        status = 'AWARDED'
    }else if (data.time.status === "HT"){
        score = '';
        status = 'HALF TIME';
        classLive = 'live_match_game_res';
    }else if (data.time.status === "LIVE"){
        score = data.scores.localteam_score +" - "+data.scores.visitorteam_score;
        status = data.time.minute +' : '+ data.time.second;
        classLive = 'live_match_game_res';
    }
    if(typeof data.round != "undefined"){
        round = '- Round '+data.round.data.name;
    }
    content  = "   " +
        "   <div class='game_mains fl_1 fl_row'>" +
        "        <div class='popup_content fl_1 fl_row'>" +
        "            <div class='game_mains_img'>" +
        "                <img src='"+data.league.data.logo_path+"' alt='flag' />" +
        "            </div>" +
        "            <div class='game_mains_text fl_1 fl_row'>" +
        // "                <div class='game_mains_country_name'>COSTA RICA:</div>" +
        "                <div class='game_mains_league_name'>"+data.league.data.name+" "+round+"</div>" +
        "                <div class='game_mains_data fl_1 fl_row_reverse'> "+data.time.starting_at.date_time+"</div>" +
        "            </div>" +
        "        </div>" +
        "    </div>" +
        "    <div class='game_teams fl_1 fl_row'>" +
        "        <div class='game_team fl_1 fl_colum'>" +
        "            <div class='game_team_logo'>" +
        "                <img src='"+data.localTeam.data.logo_path+"' alt='"+data.localTeam.data.name+"' />" +
        "            </div>" +
        "            <a target='_blank' href='/football/team/"+data.localteam_id+"' class='team_name'>"+data.localTeam.data.name+"</a>" +
        "        </div>" +
        "        <div class='game_num_result fl_colum'>" +
        "            <div class='game_result "+classLive+"'>"+score+"</div>" +
        "            <div class='game_result_status "+classLive+"'>"+status+"</div>" +
        "        </div>" +
            "        <div class='game_team fl_1 fl_colum'>" +
        "            <div class='game_team_logo'>" +
        "                <img src='"+data.visitorTeam.data.logo_path+"' alt='"+data.visitorTeam.data.name+"' />" +
        "            </div>" +
        "            <a target='_blank' href='/football/team/"+data.visitorteam_id+"' class='team_name'>"+data.visitorTeam.data.name+"</a>" +
        "        </div>" +
        "    </div>" +
        "    <div class='game_types fl_row'>" +
        "        <div class='game_type "+activeMatch+"' data-type='1'>"+languages[siteLang].matches+"</div>" +
        "        <div class='game_type "+activeH2h+"' data-type='2'>"+languages[siteLang].h2h+"</div>" +
        "        <div class='game_type "+activeStandings+"' data-type='3'>"+languages[siteLang].standing_form+"</div>" +
        "        <div class='game_type "+activeLineups+"' data-type='4'>"+languages[siteLang].lineups+"</div>" +
        "        <div class='game_type "+activeStatistics+"' data-type='5'>"+languages[siteLang].statistics+"</div>" +
        "    </div>"
        ;
    $('.game_main_props_def').html(content);
}
function setmatchSummaryContent(data) {
    // console.log('.................', data)
    var content = '';
    if(data.events.data.length == 0){
        content+= "<div class='no_match_text'>There are no results</div>"
    }else {
        if (data.time.status === "FT" || data.time.status === "LIVE" || data.time.status === "HT") {

            var firstHalfTitle = false;
            var secondHalfTitle = false;

            for (var i = 0; i < data.events.data.length; i++) {
                if (i === 0) {
                    if (data.events.data[i].minute >= 45) {
                        content += "    <div class='bage_by_half_part fl_1 fl_colum'>" +
                            "            <div class='bage_by_half_title fl_1 fl_row'>" +
                            "                <div class='fl_1'>1st Half</div>" +
                            "            </div>" +
                            "            <div class='team_datas fl_1 fl_colum'>";
                        firstHalfTitle = true;

                    }
                }

                if (firstHalfTitle == false) {
                    if (data.events.data[i].minute <= 45 || data.events.data[i].status === "HT") {
                        content += "    <div class='bage_by_half_part fl_1 fl_colum'>" +
                            "            <div class='bage_by_half_title fl_1 fl_row'>" +
                            "                <div class='fl_1'>1st Half</div>" +
                            "                <div class='fl_1'>1st Half</div>" +
                            "            </div>" +
                            "            <div class='team_datas fl_1 fl_colum'>";
                        firstHalfTitle = true;
                    }
                }
                if (secondHalfTitle == false) {
                    if (data.events.data[i].minute > 45) {
                        content += "    <div class='bage_by_half_part fl_1 fl_colum'>" +
                            "            <div class='bage_by_half_title fl_1 fl_row'>" +
                            "                <div class='fl_1'>2nd Half</div>" +
                            "                <div class='bage_by_half_result'>" + data.scores.localteam_score + " - " + data.scores.visitorteam_score + "</div>" +
                            "            </div>" +
                            "            <div class='team_datas fl_1 fl_colum'>";
                        secondHalfTitle = true;
                    }

                }

                var actionStyle = "";
                var nameStyle = "";
                var koxmClass = "";
                var p_name = "";
                var related_player_name = "";
                if (data.events.data[i].team_id == data.visitorTeam.data.id) {
                    koxmClass = "f_team_data";
                } else {
                    koxmClass = "s_team_data";
                }
                if (data.events.data[i].player_name !== null)
                    p_name = data.events.data[i].player_name;
                if (data.events.data[i].related_player_name !== null)
                    related_player_name = data.events.data[i].related_player_name;
                if (data.events.data[i].type === 'goal') {

                    actionStyle +=
                        "                    <span class='goal_own'>" +
                        "                        <img src='/images/content/football_ball.png' alt='ball' />" +
                        "                    </span>";
                    nameStyle = "<span class='player_name'>" + p_name + "</span>";
                } else if (data.events.data[i].type === "own-goal") {

                    actionStyle +=
                        "                    <span class='goal_own'>" +
                        "                        <img src='/images/content/football_ball.png' alt='ball' />" +
                        "                    </span>";
                    nameStyle = "<span class='player_name'>" + p_name + " (Own goal)</span>";
                } else if (data.events.data[i].type === "yellowcard") {
                    actionStyle = " <span class='card_type yellow_card'></span>";
                    nameStyle = "<span class='player_name'>" + p_name + "</span>";
                } else if (data.events.data[i].type === "redcard") {
                    actionStyle = " <span class='card_type red_card'></span>";
                    nameStyle = "<span class='player_name'>" + p_name + "</span>";
                } else if (data.events.data[i].type === "substitution") {
                    actionStyle = " " +
                        "<span class='substitution_c'>" +
                        "   <img src='/images/content/arrow-down-red.svg' alt='' /> " + related_player_name + " " +
                        "   <img src='/images/content/arrow-up-green.svg' alt='' /> " + p_name + "" +
                        "</span>";
                    nameStyle = " ";
                } else if (data.events.data[i].type === "penalty") {
                    var name = '';
                    nameStyle += "<span class='player_name'>Penalty <span class='goal_own'><img src='/images/content/football_ball.png' alt='ball' /></span></span>";
                    nameStyle += "<span class='player_name'>" + p_name + "</span>";
                }

                content +=
                    "                <div class='team_datas_row fl_1 " + koxmClass + "'>" +
                    "                    <span class='action_minute'>" + data.events.data[i].minute + " '</span>" +
                    " " + actionStyle + " " +
                    nameStyle + " " +
                    "                </div>";
            }
            content += "            </div>" +
                "        </div>";
            "        </div>";

        } else {
            content += "";
        }

    }

    var refery_name = "";
    var venue_name = "";
    if(typeof data.referee != "undefined"){
        refery_name = "Referee: "+data.referee.data.fullname+" , ";
    }
    if(typeof data.venue !== "undefined" && typeof data.venue.data !== "undefined"){
        venue_name = "Venue: "+data.venue.data.name+" (" +data.venue.data.city+ ")";
    }
        content += "" +
            "<div class='match_info'>" +
            "   <div class='match_info_title'>Match Information</div>" +
            "   <div class='match_info_text team_datas_row fl_1 s_team_data fl_row'>" +
            refery_name+
            venue_name +
            "   </div>" +
            "</div>";


    $('.bage_by_half_main').html(content);


}
