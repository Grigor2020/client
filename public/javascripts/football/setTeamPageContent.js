var allData = null,
    resultsData = null,
    fixturesData = null,
    transfersData = null,
    squadData = null;
$(document).ready(function () {
    //console.log('languages',languages)
    //console.log('siteLang',siteLang)
    callAllData();
    $('.liague_top_menu_item').click(function () {
        $('.liague_top_menu_item').removeClass('liague_top_menu_active_item');
        $(this).addClass('liague_top_menu_active_item');
        var type =  $(this).attr('data-type');
        if(type === '2'){
            callResults(resultsData);
        }else  if(type === '3'){
            callFixtures(fixturesData);
        }else  if(type === '4'){
            callTransfers(transfersData);
        }else{
            callSquad(squadData);
        }
    })
});

function callAllData(){
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    var teamId = $('.liga_name_s').attr('data-team-id');
    var url = '/football/team/item'
    requestPost(url,'teamId='+teamId+'',function () {
        if(this.readyState == 4){
            var result = JSON.parse(this.responseText);
            if(!result.error){
                console.log('result.data',result.data)
                $('.liague_top_menu').css({"display" : "flex"})
                resultsData = result.data.latest.data;
                fixturesData = result.data.upcoming.data;
                transfersData = result.data.transfers.data;
                squadData = sortPlayersPositions(result.data.squad.data);
                allData = result.data;
                callResults(result.data.latest.data);
            }else{
                //console.log('error')
            }
        }
    })
}



function callResults(data) {
    var content = "";
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");

        for(var i = 0; i < data.length; i++){
            var leagueInfocontent = '';
            if(i === 0){
                leagueInfocontent += "" +
                    " <div class=\"g_l_title\">\n" +
                    "                        <div class=\"check_l_name\"></div>\n" +
                    "                        <div class=\"g_liga_flag\">\n" +
                    "                            <img src='"+data[i].league.data.logo_path+"' alt=\"flag\">\n" +
                    "                        </div>\n" +
                    "                        <a href='/football/league/"+ data[i].league.data.id +"' class=\"g_liga_names\">\n" +
                    "                            <span class=\"g_liga_name\" title=\"Liga Nacional\">"+data[i].league.data.name+"</span>\n" +
                    "                        </a>\n" +
                    "                        <a href='/football/league/standing/"+ data[i].league.data.id +"' class=\"g_liga_standings  fl_row\" target=\"popup\" onclick=\"window.open('/football/league/standing/"+ data[i].league.data.id +"','popup','width=660,height=620'); return false;\">\n" +
                    languages[siteLang]['standings']+
                    "                        </a>\n" +
                    "                    </div>";
            }else{
                if(data[i].league_id !== data[i-1].league_id){
                    leagueInfocontent += "" +
                        " <div class=\"g_l_title\">\n" +
                        "                        <div class=\"check_l_name\"></div>\n" +
                        "                        <div class=\"g_liga_flag\">\n" +
                        "                            <img src='"+data[i].league.data.logo_path+"' alt=\"flag\">\n" +
                        "                        </div>\n" +
                        "                        <a href='/football/league/"+ data[i].league.data.id +"' class=\"g_liga_names\">\n" +
                        "                            <span class=\"g_liga_name\" title=\"Liga Nacional\">"+data[i].league.data.name+"</span>\n" +
                        "                        </a>\n" +
                        "                        <a href='/football/league/standing/"+ data[i].league.data.id +"' class=\"g_liga_standings  fl_row\" target=\"popup\" onclick=\"window.open('/football/league/standing/"+ data[i].league.data.id +"','popup','width=660,height=620'); return false;\">\n" +
                        languages[siteLang]['standings']+
                        "                        </a>\n" +
                        "                    </div>";
                }
            }

            var htContent = "";
            var scoreContent = '';
            var ht_hash = [];
            var liveClass = "";
            if(
                data[i].time.status == "SUSP" ||
                data[i].time.status == "POSTP" ||
                data[i].time.status == "DELAYED" ||
                data[i].time.status == "TBA" ||
                data[i].time.status == "CANCL"
            ){
                if(data[i].scores.ht_score !== null){
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div>"+data[i].time.status+"</div>";
                }
            }else if(data[i].time.status == "AWARDED"){
                htContent += "<div class='round_game_1_team_result'>"+data[i].time.status+"</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>"+data[i].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result'>-</div>" +
                    "<div class='round_game_1_team_result'>"+data[i].scores.visitorteam_score+"</div>";
            }else if(data[i].time.status == "HT"){
                liveClass = "live_match_game_res";
                if(data[i].scores.ht_score !== null){
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result "+liveClass+"'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                        "    <div class='round_game_1_team_result  "+liveClass+"'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div class='live_match_game_res'>"+data[i].time.minute+"'</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result xx "+liveClass+"'>-</div>" +
                    "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].scores.visitorteam_score+"</div>";
            }else if(data[i].time.status == "NS"){
                htContent += "<div class=''>"+data[i].time.status+"</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>-</div>";
            }else if(data[i].time.status == "LIVE"){
                liveClass = "live_match_game_res";
                htContent += "<div class='live_match_game_res'>"+data[i].time.status+" "+data[i].time.minute+"'</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                    "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].scores.visitorteam_score+"</div>";
            }else if(data[i].time.status == "FT"){
                if(data[i].scores.ht_score !== null){
                    ht_hash = data[i].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div>"+data[i].time.status+"</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result  '>"+data[i].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result  '>-</div>" +
                    "<div class='round_game_1_team_result  '>"+data[i].scores.visitorteam_score+"</div>";

            }


            // htContent += "<div class='round_game_1_team_result'>"+data[i].time.status+"</div>";
            content +=leagueInfocontent+"   <a href='/match/"+data[i].id+"' class='result_content  round_row fl_1 fl_row'  target='popup'" +
                " onclick=\"window.open('/football/match/"+data[i].id+"','popup','width=660,height=620'); return false;\">" +
                // roundContent += "             <div class=' round_row fl_1 fl_row'>" +
                "                            <div class='round_game_data'>"+data[i].time.starting_at.date_time+"</div>" +
                "                            <div class='round_game_teams fl_row'>" +
                // "                                    <a href='/football/team/"+data[i].localTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].localTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].localTeam.data.name+"</div>" +
                scoreContent+
                // "                                    <a href='/football/team/"+data[i].visitorTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row'>"+data[i].visitorTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row'>"+data[i].visitorTeam.data.name+"</div>" +
                "                            </div>" +
                "<div class='round_game_ht fl_row'>" +
                htContent+
                "</div>"+
                "                        </a>"
        }

    $('.page_result_data').html(content);
}

function callFixtures(data) {
    //console.log('data',data)
    var content = "";
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");

    for(var i = 0; i < data.length; i++){
        var leagueInfocontent = '';
        if(i === 0){
            leagueInfocontent += "" +
                " <div class=\"g_l_title\">\n" +
                "                        <div class=\"check_l_name\"></div>\n" +
                "                        <div class=\"g_liga_flag\">\n" +
                "                            <img src='"+data[i].league.data.logo_path+"' alt=\"flag\">\n" +
                "                        </div>\n" +
                "                        <a href='/football/league/"+ data[i].league.data.id +"' class=\"g_liga_names\">\n" +
                "                            <span class=\"g_liga_name\" title=\"Liga Nacional\">"+data[i].league.data.name+"</span>\n" +
                "                        </a>\n" +
                "                        <a href='/football/league/standing/"+ data[i].league.data.id +"' class=\"g_liga_standings  fl_row\" target=\"popup\" onclick=\"window.open('/football/league/standing/"+ data[i].league.data.id +"','popup','width=660,height=620'); return false;\">\n" +
                languages[siteLang]['standings']+
                "                        </a>\n" +
                "                    </div>";
        }else{
            if(data[i].league_id !== data[i-1].league_id){
                leagueInfocontent += "" +
                    " <div class=\"g_l_title\">\n" +
                    "                        <div class=\"check_l_name\"></div>\n" +
                    "                        <div class=\"g_liga_flag\">\n" +
                    "                            <img src='"+data[i].league.data.logo_path+"' alt=\"flag\">\n" +
                    "                        </div>\n" +
                    "                        <a href='/football/league/"+ data[i].league.data.id +"' class=\"g_liga_names\">\n" +
                    "                            <span class=\"g_liga_name\" title=\"Liga Nacional\">"+data[i].league.data.name+"</span>\n" +
                    "                        </a>\n" +
                    "                        <a href='/football/league/standing/"+ data[i].league.data.id +"' class=\"g_liga_standings  fl_row\" target=\"popup\" onclick=\"window.open('/football/league/standing/"+ data[i].league.data.id +"','popup','width=660,height=620'); return false;\">\n" +
                    languages[siteLang]['standings']+
                    "                        </a>\n" +
                    "                    </div>";
            }
        }

        var htContent = "";
        var scoreContent = '';
        var ht_hash = [];
        var liveClass = "";
        var timeText = data[i].time.starting_at.date_time;
        if(
            data[i].time.status == "SUSP" ||
            data[i].time.status == "POSTP" ||
            data[i].time.status == "DELAYED" ||
            data[i].time.status == "TBA" ||
            data[i].time.status == "CANCL"
        ){
            timeText = "---";
            if(data[i].scores.ht_score !== null){
                ht_hash = data[i].scores.ht_score.split('');
                htContent +=
                    "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                    "    <div class='round_game_1_team_result'>-</div>" +
                    "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
            }else{
                htContent += "<div>"+data[i].time.status+"</div>";
            }
            scoreContent +=
                "<div class='round_game_1_team_result'></div>" +
                "<div class='round_game_1_team_result'>-</div>" +
                "<div class='round_game_1_team_result'></div>";
        }else if(data[i].time.status == "AWARDED"){
            htContent += "<div class='round_game_1_team_result'>"+data[i].time.status+"</div>";
            scoreContent +=
                "<div class='round_game_1_team_result'>"+data[i].scores.localteam_score+"</div>" +
                "<div class='round_game_1_team_result'>-</div>" +
                "<div class='round_game_1_team_result'>"+data[i].scores.visitorteam_score+"</div>";
        }else if(data[i].time.status == "HT"){
            liveClass = "live_match_game_res";
            if(data[i].scores.ht_score !== null){
                ht_hash = data[i].scores.ht_score.split('');
                htContent +=
                    "    <div class='round_game_1_team_result "+liveClass+"'>"+ht_hash[0]+"</div>" +
                    "    <div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                    "    <div class='round_game_1_team_result  "+liveClass+"'>"+ht_hash[2]+"</div>"
            }else{
                htContent += "<div class='live_match_game_res'>"+data[i].time.minute+"'</div>";
            }
            scoreContent +=
                "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].scores.localteam_score+"</div>" +
                "<div class='round_game_1_team_result xx "+liveClass+"'>-</div>" +
                "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].scores.visitorteam_score+"</div>";
        }else if(data[i].time.status == "NS"){
            htContent += "<div class=''>"+data[i].time.status+"</div>";
            scoreContent +=
                "<div class='round_game_1_team_result'>-</div>";
        }else if(data[i].time.status == "LIVE"){
            liveClass = "live_match_game_res";
            htContent += "<div class='live_match_game_res'>"+data[i].time.status+" "+data[i].time.minute+"'</div>";
            scoreContent +=
                "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].scores.localteam_score+"</div>" +
                "<div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].scores.visitorteam_score+"</div>";
        }else if(data[i].time.status == "FT"){
            if(data[i].scores.ht_score !== null){
                ht_hash = data[i].scores.ht_score.split('');
                htContent +=
                    "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                    "    <div class='round_game_1_team_result'>-</div>" +
                    "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
            }else{
                htContent += "<div>"+data[i].time.status+"</div>";
            }
            scoreContent +=
                "<div class='round_game_1_team_result  '>"+data[i].scores.localteam_score+"</div>" +
                "<div class='round_game_1_team_result  '>-</div>" +
                "<div class='round_game_1_team_result  '>"+data[i].scores.visitorteam_score+"</div>";

        }


        // htContent += "<div class='round_game_1_team_result'>"+data[i].time.status+"</div>";
        content +=leagueInfocontent+"   <a href='/match/"+data[i].id+"' class='result_content  round_row fl_1 fl_row'  target='popup'" +
            " onclick=\"window.open('/football/match/"+data[i].id+"','popup','width=660,height=620'); return false;\">" +
            // roundContent += "             <div class=' round_row fl_1 fl_row'>" +
            "                            <div class='round_game_data'>"+timeText+"</div>" +
            "                            <div class='round_game_teams fl_row'>" +
            // "                                    <a href='/football/team/"+data[i].localTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].localTeam.data.name+"</a>" +
            "                                    <div  class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].localTeam.data.name+"</div>" +
            scoreContent+
            // "                                    <a href='/football/team/"+data[i].visitorTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row'>"+data[i].visitorTeam.data.name+"</a>" +
            "                                    <div  class='round_game_1_team_name fl_1 fl_row'>"+data[i].visitorTeam.data.name+"</div>" +
            "                            </div>" +
            "<div class='round_game_ht fl_row'>" +
            htContent+
            "</div>"+
            "                        </a>"
    }

    $('.page_result_data').html(content);
}

function callTransfers(data) {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");

    var content = "";

    content += " <div class=\"transfers_main\">\n" +
        "                        <div class=\"transfers_title fl_row fl_1\">\n" +
        "                            <div class=\"transfer_date\">Date</div>\n" +
        "                            <div class=\"transfer_team fl_row fl_1\">Player</div>\n" +
        "                            <div class=\"transfer_team fl_row fl_1\">Type</div>\n" +
        "                            <div class=\"transfer_team fl_row fl_1\">From / To</div>\n" +
        "                        </div>";
    for(var i = 0; i < data.length; i++){
        var img = '';
        var toTeamInfo = '';

        if(data[i].type === "IN"){
            img = " <img src='/images/content/long-arrow-alt-left-green.svg' alt='IN' />";
        }
        if(data[i].type === "OUT"){
            img = " <img src='/images/content/long-arrow-alt-right-red.svg' alt='OUT' />";
        }

        if(typeof data[i].team != "undefined"){
            toTeamInfo =  "        <img src='"+data[i].team.data.logo_path+"' alt='"+data[i].team.data.name+"' />" +
                data[i].team.data.name ;
        }

        var transferAmount = "";

        if(data[i].amount !== null){
            transferAmount = " "+data[i].amount
        }
        content +=
            "<div class='transfers_row fl_row fl_1'>" +
            "    <div class='transfer_date'>"+data[i].date+"</div>" +
            "    <div class='transfer_team fl_row fl_1'>"+data[i].player.data.display_name+" ("+data[i].player.data.birthcountry+")</div>" +
            "    <div class='transfer_team fl_row fl_1'>" +
            img +
            data[i].transfer +" "+transferAmount+
            "    </div>" +
            "    <div class='transfer_team fl_row fl_1'>" +
            toTeamInfo+
            "    </div>" +
            "</div>";
    }

    content += "</div>";
    $('.page_result_data').html(content);
}

function callSquad() {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(squadData !== null){
        setSquadContent(squadData);
    }
}
function sortPlayersPositions(data) {
    data.sort(compare);

    return data;
}



function setSquadContent(data) {
    var content = "";
    content += "<div class=\"team_squad\">\n" +
        "                        <div class=\"team_squad_title fl_1 fl_row\">\n" +
        "                            <div class=\"team_squad_num\">#</div>\n" +
        "                            <div class=\"team_squad_name fl_1 fl_row\">Name</div>\n" +
        "                            <div class=\"team_squad_age\">Birthdate</div>\n" +
        "                            <div class=\"team_squad_lineups\"><img src=\"/images/content/tshirt-green.svg\"></div>\n" +
        "                            <div class=\"team_squad_goals\"><img src=\"/images/content/football_ball.png\" alt=\"ball\"></div>\n" +
        "                            <div class=\"team_squad_cards\"><span class=\"card_type yellow_card\"></span></div>\n" +
        "                            <div class=\"team_squad_cards\"><span class=\"card_type red_card\"></span></div>\n" +
        "                        </div>\n";
    var checkGoalkeeper = false,
        checkDefender = false,
        checkMidfielder = false,
        checkAttacker = false;
    for(var i = 0; i < data.length; i++){

        var goalkeeperTitle =
            "<div class='team_squad_position fl_1 fl_row'>" +
            "    <div class='team_squad_name fl_1 fl_row'>Goalkeeper</div>" +
            "</div>";
        var defenderTitle =
            "<div class='team_squad_position fl_1 fl_row'>" +
            "    <div class='team_squad_name fl_1 fl_row'>Defender</div>" +
            "</div>";
        var midfielderTitle =
            "<div class='team_squad_position fl_1 fl_row'>" +
            "    <div class='team_squad_name fl_1 fl_row'>Midfielder</div>" +
            "</div>";
        var attackerTitle =
            "<div class='team_squad_position fl_1 fl_row'>" +
            "    <div class='team_squad_name fl_1 fl_row'>Attacker</div>" +
            "</div>";
        var titleContent = "";
        if(data[i].position_id === 1 && checkGoalkeeper === false){
            titleContent = goalkeeperTitle;
            checkGoalkeeper = true;
        }else if (data[i].position_id === 2 && checkDefender === false){
            titleContent = defenderTitle;
            checkDefender = true;
        }else if (data[i].position_id === 3 && checkMidfielder === false){
            titleContent = midfielderTitle;
            checkMidfielder = true;
        }else if (data[i].position_id === 4 && checkAttacker === false){
            titleContent = attackerTitle;
            checkAttacker = true;
        }
        var number = '',
            bday = '',
            lineups = '',
            goals = '',
            yellowcards = '',
            redcards = '';
        if(data[i].number !== null && typeof data[i].number !== "undefined")
            number = data[i].number
        if(data[i].player.data.birthdate !== null && typeof data[i].player.data.birthdate !== "undefined")
            bday = data[i].player.data.birthdate
        if(data[i].lineups !== null && typeof data[i].lineups !== "undefined")
            lineups = data[i].lineups
        if(data[i].goals !== null && typeof data[i].goals !== "undefined")
            goals = data[i].goals
        if(data[i].yellowcards !== null && typeof data[i].yellowcards !== "undefined")
            yellowcards = data[i].yellowcards
        if(data[i].redcards !== null && typeof data[i].redcards !== "undefined")
            redcards = data[i].redcards
        content +=
            titleContent +
            "                        <div class='team_squad_row fl_1 fl_row'>" +
            "                            <div class='team_squad_num'>"+number+"</div>" +
            "                            <a href='/football/player/"+data[i].player_id+"' class='team_squad_name fl_1 fl_row' title='"+data[i].player.data.fullname+"'>"+data[i].player.data.display_name+"</a>" +
            "                            <div class='team_squad_age'>"+bday+"</div>" +
            "                            <div class='team_squad_lineups'>"+lineups+"</div>" +
            "                            <div class='team_squad_goals'>"+goals+"</div>" +
            "                            <div class='team_squad_cards'>"+yellowcards+"</div>" +
            "                            <div class='team_squad_cards'>"+redcards+"</div>" +
            "                        </div>";
    }


    content += "</div>";
    $('.page_result_data').html(content);

}
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const A = a.position_id;
    const B = b.position_id;

    let comparison = 0;
    if (A > B) {
        comparison = 1;
    } else if (A < B) {
        comparison = -1;
    }
    return comparison;
}
