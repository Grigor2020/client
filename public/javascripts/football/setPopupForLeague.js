var standingsData = null,
    topscorersData = null;

$(document).ready(function () {
    $('.game_type').click(function () {
        $('.games_by_liga').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
        $('.game_type').removeClass('active_game_type');
        $(this).addClass('active_game_type');
        var type = $(this).attr('data-type');
        if (type === '1') {
            callStanding();
        }
        if (type === '2') {
            callTopscorers();
        }
        if (type === '3') {
            callTopAssists();
        }
    });

    callStanding();
    function callStanding(){
        if(standingsData === null){
            var url = '/football/league/standing';
            var leagueId = $('#league_id_p').val();
            requestPost(url,'league_id='+leagueId,function () {
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(!result.error){
                        //console.log('result.dataresult.dataresult.data',result.data)
                        if(result.data.length === 0){
                            alert('No Info')
                            return false;
                        }
                        standingsData = result.data[0];
                        setStandingsPopupContent(standingsData);
                    }else{
                        setStandingsPopupContent([]);
                    }
                }
            })
        }else {
            setStandingsPopupContent(standingsData);
        }
    }

    function callTopAssists(){
        setTopAssistsContent(topscorersData);
    }
    function callTopscorers(){
        if(topscorersData === null){
            var url = '/football/league/topscorers';
            var leagueId = $('#league_id_p').val();
            requestPost(url,'league_id='+leagueId,function () {
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(!result.error){
                        //console.log('XXXX',result.data)
                        topscorersData = result.data;
                        setTopscorersPopupContent(topscorersData);
                    }else{
                        setTopscorersPopupContent([]);
                    }
                }
            })
        }else {
            setTopscorersPopupContent(topscorersData);
        }
    }


    function setTopscorersPopupContent(data) {
        //console.log('setTopscorersPopupContent',data)
        var content = [];
        var list = data.goalscorers.data;
        //console.log('goalscorers list',list)
        content.push("" +
            "<div class=\"standings_title fl_1 fl_row\">\n" +
            "                            <div class=\"standings_t_num\">#</div>\n" +
            "                            <div class=\"standings_t_name fl_1 fl_row\">Name</div>\n" +
            "                            <div class=\" fl_1 fl_row\">Team</div>\n" +
            "                            <div class=\"standings_t_form\">Goals</div>\n" +
            "                        </div>");
        for(var i = 0; i < list.length; i++){

            content.push(
                " <div class='standings_row fl_1 fl_row'>" +
                "    <div class='standings_num ' title='"+list[i].player.data.fullname+"'>"+list[i].position+".</div>" +
                "    <a target='_blank' href='/football/player/"+list[i].player.data.player_id+"' class='standings_name fl_1 fl_row'>" +
                list[i].player.data.fullname   +
                "    </a>" +
                "    <a target='_blank' href='/football/team/"+list[i].team.data.id+"' class=' fl_1 fl_row'>" +
                list[i].team.data.name   +
                "    </a>" +
                "    <div class='standings_t_form'>"+list[i].goals+"  ("+list[i].penalty_goals+")</div>" +
                " </div>"
            )
        }

        $('.games_by_liga').html(content)
    }



    function setTopAssistsContent(data) {
        //console.log('setTopscorersPopupContent',data)
        var content = [];
        var list = data.assistscorers.data;
        //console.log('goalscorers list',list)
        content.push("" +
            "<div class=\"standings_title fl_1 fl_row\">\n" +
            "                            <div class=\"standings_t_num\">#</div>\n" +
            "                            <div class=\"standings_t_name fl_1 fl_row\">Name</div>\n" +
            "                            <div class=\" fl_1 fl_row\">Team</div>\n" +
            "                            <div class=\"standings_t_form\">Assists</div>\n" +
            "                        </div>");
        for(var i = 0; i < list.length; i++){

            content.push(
                " <div class='standings_row fl_1 fl_row'>" +
                "    <div class='standings_num ' title='"+list[i].player.data.fullname+"'>"+list[i].position+".</div>" +
                "    <a target='_blank' href='/football/player/"+list[i].player.data.player_id+"' class='standings_name fl_1 fl_row'>" +
                list[i].player.data.fullname   +
                "    </a>" +
                "    <a target='_blank' href='/football/team/"+list[i].team.data.id+"' class=' fl_1 fl_row'>" +
                list[i].team.data.name   +
                "    </a>" +
                "    <div class='standings_t_form'>"+list[i].assists+"</div>" +
                " </div>"
            )
        }

        $('.games_by_liga').html(content)
    }



    function setStandingsPopupContent(data) {
        var content = [];
        var list = data.standings.data;
        //console.log('list',list)
        content.push("" +
            "<div class=\"standings_title fl_1 fl_row\">\n" +
            "                            <div class=\"standings_t_num\">#</div>\n" +
            "                            <div class=\"standings_t_name fl_1 fl_row\">Team</div>\n" +
            "                            <div class=\"standings_t_mp\">MP</div>\n" +
            "                            <div class=\"standings_t_w\">W</div>\n" +
            "                            <div class=\"standings_t_d\">D</div>\n" +
            "                            <div class=\"standings_t_l\">L</div>\n" +
            "                            <div class=\"standings_t_g\">G</div>\n" +
            "                            <div class=\"standings_t_pts\">Pts</div>\n" +
            "                            <div class=\"standings_t_form\">Form</div>\n" +
            "                        </div>");
        for(var i = 0; i < list.length; i++){
            var positionColorClass = "";
            if(list[i].position === 1 || list[i].position === 2 ||list[i].position === 3 ||list[i].position === 4 ){
                positionColorClass = "num_color_blue";
            }else if (list[i].position === 5 || list[i].position === 6){
                positionColorClass = "num_color_darkred";
            }else if (list[i].position === 7){
                positionColorClass = "num_color_pink";
            }else if (list[i].position === 18 || list[i].position === 19 || list[i].position === 20){
                positionColorClass = "num_color_red";
            }
            let gameResCont = "";
            let game_results = list[i].recent_form.split("");
            for(let j = 0;  j < game_results.length; j++){
                let colorClass = "";
                if(game_results[j] === "W"){
                    colorClass = "item_win";
                }  else if (game_results[j] === "D"){
                    colorClass = "item_no_one";
                }else {
                    colorClass = "item_loose";
                }
                gameResCont += "<div class='standings_form_item "+colorClass+"'>"+game_results[j]+"</div>";
            }

            content.push(
                " <div class='standings_row fl_1 fl_row'>" +
                "    <div class='standings_num "+positionColorClass+"' title='"+list[i].result+"'>"+list[i].position+".</div>" +
                "    <a target='_blank' href='/football/team/"+list[i].team_id+"' class='standings_name fl_1 fl_row'>" +
                list[i].team_name   +
                "    </a>" +
                "    <div class='standings_mp'>"+list[i].overall.games_played+"</div>" +
                "    <div class='standings_w'>"+list[i].overall.won+"</div>" +
                "    <div class='standings_d'>"+list[i].overall.draw+"</div>" +
                "    <div class='standings_l'>"+list[i].overall.lost+"</div>" +
                "    <div class='standings_g'>"+list[i].overall.goals_scored+" : "+list[i].overall.goals_against+"</div>" +
                "    <div class='standings_pts'>"+list[i].points+"</div>" +
                "    <div class='standings_form fl_row'>" +
                gameResCont+
                "     </div>" +
                " </div>"
            )
        }


        content.push("<div class=\"promotion_colors fl_1 fl_colum\">\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_blue\"></div> Promotion - Group Stage\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_darkred\"></div> Promotion - Group Stage\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_pink\"></div> Promotion - Qualification\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_red\"></div> Relegation" +
            "                    </div>\n" +
            "                </div>\n" +
            "                <div class=\"end_page_text\">\n" +
            "                    If teams finish on equal points at the end of the season, head-to-head matches will be the tie-breaker.\n" +
            "                </div>");

        $('.games_by_liga').html(content)
    }
})


