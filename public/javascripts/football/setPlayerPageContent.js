var summaryData = null;

$(document).ready(function () {
    callSummary();
});


function callSummary() {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    // console.log('callStandings')
    var playerId = $('.liga_name_s').attr('data-player-id');
    var url = '/football/player/get-player'
    requestPost(url,'playerId='+playerId+'',function () {
        // if(this.)
        if(this.readyState == 4){
            var result = JSON.parse(this.responseText);
            if(!result.error){
                summaryData = result.data;
                setMainContent(summaryData)
                setContent(summaryData)
            }else{
                //console.log('error')
            }
        }
    })
}
function setMainContent(data) {
    $('.pl_country').text(data.birthcountry);
    $('.pl_name').text(data.display_name);
    $('.pl_birthdate').text(data.birthdate);
    $('.pl_position').text(data.position.data.name);
    $('.pl_team').text("( "+data.team.data.name+" )");
    $('.pl_img').attr("src",data.image_path);
    $('.pl_team_logo').attr("src",data.team.data.logo_path);
    $('.team_hr').attr("href","/football/team/"+data.team.data.id);
}

function setContent(data){
    var resContent = '';
    var transfersContent = "";
    var injuryContent = "";
    var statsContent = "";

    if(data.stats.data.length > 0){
        var statsData = data.stats.data;
        statsContent += "" +
            "<div class=\"liague_top_menu fl_1 fl_row inj_row\">\n" +
            "                        <div class=\"liague_top_menu_item liague_top_menu_active_item\">Career\n</div>\n" +
            "                    </div>" +
            "<div class=\"team_squad\">\n" +
            "                        <div class=\"team_squad_row fl_1 fl_row\">\n" +
            "                            <div class=\"team_squad_name fl_1 fl_row inj_row_p\">Season</div>\n" +
            "                            <div class=\"team_squad_name fl_1 fl_row inj_row_p\">Team</div>\n" +
            "                            <div class=\"team_squad_name fl_1 fl_row inj_row_p\">Competition</div>\n" +
            "                            <div class=\"team_squad_lineups\"><img src=\"/images/content/tshirt-green.svg\" alt=''></div>\n" +
            "                            <div class=\"team_squad_goals\"><img src=\"/images/content/football_ball.png\" alt=\"ball\"></div>\n" +
            "                            <div class=\"team_squad_cards\"><span class=\"card_type yellow_card\"></span></div>\n" +
            "                            <div class=\"team_squad_cards\"><span class=\"card_type red_card\"></span></div>\n" +
            "                        </div>\n";
        for(var i = 0; i < statsData.length; i++){
            statsContent +="                        <div class='team_squad_title fl_1 fl_row'>";
            if(typeof statsData[i].season !== "undefined"){
                statsContent +="<div class='team_squad_name fl_1 fl_row inj_row_p'>"+statsData[i].season.data.name+"</div>";
            }else{
                statsContent +="<div class='team_squad_name fl_1 fl_row inj_row_p'></div>";
            }
            statsContent +="                            <a href='/football/team/"+statsData[i].team.data.id+"' class='team_squad_name fl_1 fl_row' >" +
                "<img class='team_squad_logo' src='"+statsData[i].team.data.logo_path+"' alt=''>"+
                statsData[i].team.data.name+
                "</a>";
            if(typeof statsData[i].league !== "undefined"){
                statsContent +=" <div class='team_squad_name fl_1 fl_row'>"+statsData[i].league.data.name+"</div>";
            }else {
                statsContent +=" <div class='team_squad_name fl_1 fl_row'></div>";
            }



            statsContent +="                            <div class='team_squad_lineups'>"+statsData[i].lineups+"</div>" +
            "                            <div class='team_squad_goals'>"+statsData[i].goals+"</div>" +
            "                            <div class='team_squad_cards'>"+statsData[i].yellowcards+"</div>" +
            "                            <div class='team_squad_cards'>"+statsData[i].redcards+"</div>" +
            "                        </div>";
        }
        statsContent += "</div>";
        resContent += statsContent;
    }

    if(data.transfers.data.length > 0){
        var transfersData = sortTransferDates(data.transfers.data).reverse();


        transfersContent += "" +
            "  <div class=\"liague_top_menu fl_1 fl_row\">\n" +
            "                    <div class=\"liague_top_menu_item liague_top_menu_active_item inj_row\">Transfers</div>\n" +
            "                </div>";

        transfersContent += " <div class=\"transfers_main\">\n" +
            "                        <div class=\"transfers_title fl_row fl_1\">\n" +
            "                            <div class=\"transfer_date\">Date</div>\n" +
            "                            <div class=\"transfer_team fl_row fl_1\">Type</div>\n" +
            "                            <div class=\"transfer_team fl_row fl_1\">From / To</div>\n" +
            "                        </div>";
        for(var i = 0; i < transfersData.length; i++){
            var img = '';
            var toTeamInfo = '';

            if(transfersData[i].type === "OUT"){
                img = " <img src='/images/content/long-arrow-alt-left-green.svg' alt='IN' />";
            }
            if(transfersData[i].type === "IN"){
                img = " <img src='/images/content/long-arrow-alt-right-red.svg' alt='OUT' />";
            }

            if(typeof transfersData[i].team != "undefined"){
                toTeamInfo =  "        <img src='"+transfersData[i].team.data.logo_path+"' alt='"+transfersData[i].team.data.name+"' />" +
                    "<a href='/football/team/"+ transfersData[i].team.data.id +"'>"+transfersData[i].team.data.name+"</a>"
            }

            var transferAmount = "";

            if(transfersData[i].amount !== null){
                transferAmount = " "+transfersData[i].amount
            }
            transfersContent +=
                "<div class='transfers_row fl_row fl_1'>" +
                "    <div class='transfer_date'>"+transfersData[i].date+"</div>" +
                "    <div class='transfer_team fl_row fl_1'>" +
                img +
                transferAmount+
                "    </div>" +
                "    <div class='transfer_team fl_row fl_1'>" +
                toTeamInfo+
                "    </div>" +
                "</div>";
        }
        transfersContent += "</div>";
        resContent += transfersContent;
    }

    if(data.sidelined.data.length > 0){
        var injuryData = data.sidelined.data.reverse();
        injuryContent += "" +
            "<div class=\"liague_top_menu fl_1 fl_row inj_row\">\n" +
            "                        <div class=\"liague_top_menu_item liague_top_menu_active_item\">Injury History\n</div>\n" +
            "                    </div>" +
            " <div class=\"transfers_main\">\n" +
            "                        <div class=\"transfers_title fl_row fl_1\">\n" +
            "                            <div class=\"fl_1 inj_row_p\">From</div>\n" +
            "                            <div class=\"fl_1 inj_row_p\">Until</div>\n" +
            "                            <div class=\"fl_1 inj_row_p\">Injury</div>\n" +
            "                        </div>";
        for(var i = 0; i < injuryData.length; i++){
                injuryContent += "" +
                    " <div class=\"transfers_row fl_row fl_1\">\n" +
                    "     <div class=\"fl_1 inj_row_p\">"+injuryData[i].start_date+"</div>\n" +
                    "     <div class=\"fl_1 inj_row_p\">"+injuryData[i].end_date+"</div>\n" +
                    "     <div class=\"fl_1 inj_row_p\">"+injuryData[i].description+"</div>\n" +
                    " </div>";
        }
        injuryContent += "</div>";
        resContent += injuryContent;
    }


    $('.page_result_data').html(resContent);
}





function sortTransferDates(data) {
    data.sort(compare);
    return data;
}
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const A = a.date;
    const B = b.date;

    let comparison = 0;
    if (A > B) {
        comparison = 1;
    } else if (A < B) {
        comparison = -1;
    }
    return comparison;
}

