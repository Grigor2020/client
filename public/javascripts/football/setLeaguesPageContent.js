var resultsData = null,
    fixturesData = null,
    standingsData = null,
    archiveData = null,
    summaryData = null;
$(document).ready(function () {
    callResults();
    $('.liague_top_menu_item').click(function () {
        $('.liague_top_menu_item').removeClass('liague_top_menu_active_item');
        $(this).addClass('liague_top_menu_active_item');
        var type =  $(this).attr('data-type');
        if(type === '1'){
            // callSummary();
        }else  if(type === '2'){
            callResults();
        }else  if(type === '3'){
            callFixtures();
        }else  if(type === '4'){
            callStandings();
        }else{
            callArchive();
        }
    })
});

// function callSummary() {
//     $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
// }
function callResults() {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(resultsData === null){
        var seasonId = $('.liga_name_s').attr('data-season-id');
        var leagueId = $('.liga_name_s').attr('data-league-id');
        var url = '/football/league/results'
        requestPost(url,'seasonId='+seasonId+'&leagueId='+leagueId,function () {
            // if(this.)
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);

                if(!result.error){
                    var resArr = [];
                    var fixArr = [];
                    // console.log('........................',result.data)
                    for(var i = 0; i < result.data.length; i++){
                        if(result.data[i].results.data.length > 0){
                            resArr.push(result.data[i])
                        }else{
                            fixArr.push(result.data[i])
                        }
                        // if(result.data[i].fixtures.data.length > 0){
                        //
                        // }
                    }
                    console.log('........................ resArr',resArr)
                    console.log('........................ fixArr',fixArr)
                    resultsData = resArr;
                    fixturesData = fixArr;
                    setResultsContent(resArr)
                }else{
                    //console.log('error')
                }
            }
        })
    }else{
        setResultsContent(resultsData)
    }

}
function callFixtures() {
    setFixturesContent(fixturesData)
}
function callArchive() {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
}

function callStandings() {
    $('.page_result_data').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
    if(!standingsData){
        var seasonId = $('.liga_name_s').attr('data-season-id');
        var url = '/football/league/standings'
        requestPost(url,'seasonId='+seasonId+'',function () {
            // if(this.)
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    // console.log('result.data',result.data)
                    standingsData = result.data;
                    setStandingsContent(standingsData)
                }else{
                    //console.log('error')
                }
            }
        })
    }else{
        setStandingsContent(standingsData)
    }
}
function setFixturesContent(data) {
    // console.log('setFixturesContent',data)
    var content = [];
    if(data.length === 0){
        content.push("<div class='no_match_text'>No match found.</div>")
    }
    for(var i = 0; i <data.length  ; i++){
        var current_round_id = data[0].league.data.current_round_id ;
        var id = '';
        var current_text = '';
        var current_class = '';
        if(data[i].id == current_round_id){
            id = "id='r_"+data[i].league.data.current_round_id+"'";
            current_text = "(current)";
            current_class = "current_round_cl";
        }

        var roundContent = '';
        roundContent += "<div class='result_content fl_1 fl_colum'>";
        roundContent += "                        <div class=' "+current_class+" round_title fl_1 fl_row' "+id+" >" +
            "                            <div class='round_name_text'>"+ languages[siteLang].round +" "+data[i].name+" "+current_text+"</div>" +
            "                        </div>";
        for(var j = 0; j < data[i].fixtures.data.length; j++){
            var timeText = data[i].fixtures.data[j].time.starting_at.date_time;
            var htContent = "";
            var scoreContent = '';
            var ht_hash = [];
            var liveClass = "";
            if(
                data[i].fixtures.data[j].time.status == "SUSP" ||
                data[i].fixtures.data[j].time.status == "POSTP" ||
                data[i].fixtures.data[j].time.status == "DELAYED" ||
                data[i].fixtures.data[j].time.status == "TBA" ||
                data[i].fixtures.data[j].time.status == "CANCL"
            ){
                timeText = "---";
                if(data[i].fixtures.data[j].scores.ht_score !== null){
                    ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div>"+data[i].fixtures.data[j].time.status+"</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result'></div>" +
                    "<div class='round_game_1_team_result'>-</div>" +
                    "<div class='round_game_1_team_result'></div>";
            }else if(data[i].fixtures.data[j].time.status == "AWARDED"){
                htContent += "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].time.status+"</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result'>-</div>" +
                    "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
            }else if(data[i].fixtures.data[j].time.status == "HT"){
                liveClass = "live_match_game_res";
                if(data[i].fixtures.data[j].scores.ht_score !== null){
                    ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result "+liveClass+"'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                        "    <div class='round_game_1_team_result  "+liveClass+"'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div class='live_match_game_res'>"+data[i].fixtures.data[j].time.minute+"'</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result xx "+liveClass+"'>-</div>" +
                    "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
            }else if(data[i].fixtures.data[j].time.status == "NS"){
                htContent += "<div class=''>"+data[i].fixtures.data[j].time.status+"</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result'>-</div>";
            }else if(data[i].fixtures.data[j].time.status == "LIVE"){
                liveClass = "live_match_game_res";
                htContent += "<div class='live_match_game_res'>"+data[i].fixtures.data[j].time.status+" "+data[i].fixtures.data[j].time.minute+"'</div>";
                scoreContent +=
                    "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                    "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
            }else if(data[i].fixtures.data[j].time.status == "FT"){
                if(data[i].fixtures.data[j].scores.ht_score !== null){
                    ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                    htContent +=
                        "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                        "    <div class='round_game_1_team_result'>-</div>" +
                        "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                }else{
                    htContent += "<div>"+data[i].fixtures.data[j].time.status+"</div>";
                }
                scoreContent +=
                    "<div class='round_game_1_team_result  '>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                    "<div class='round_game_1_team_result  '>-</div>" +
                    "<div class='round_game_1_team_result  '>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";

            }


            // htContent += "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].time.status+"</div>";
            roundContent +="   <a href='/match/"+data[i].fixtures.data[j].id+"' class=' round_row fl_1 fl_row'  target='popup'" +
                " onclick=\"window.open('/football/match/"+data[i].fixtures.data[j].id+"','popup','width=660,height=620'); return false;\">" +
                // roundContent += "             <div class=' round_row fl_1 fl_row'>" +
                "                            <div class='round_game_data'>"+timeText+"</div>" +
                "                            <div class='round_game_teams fl_row'>" +
                // "                                    <a href='/football/team/"+data[i].fixtures.data[j].localTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].fixtures.data[j].localTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].fixtures.data[j].localTeam.data.name+"</div>" +
                scoreContent+
                // "                                    <a href='/football/team/"+data[i].fixtures.data[j].visitorTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row'>"+data[i].fixtures.data[j].visitorTeam.data.name+"</a>" +
                "                                    <div  class='round_game_1_team_name fl_1 fl_row'>"+data[i].fixtures.data[j].visitorTeam.data.name+"</div>" +
                "                            </div>" +
                "<div class='round_game_ht fl_row'>" +
                htContent+
                "</div>"+
                "                        </a>"
        }
        roundContent += '</div>';
        content.push(roundContent);
    }
    $('.page_result_data').html(content);
}
function setResultsContent(data) {
    var content = [];
    if(data.length > 0){

        var current_round_id = data[0].league.data.current_round_id;
        for(var i = data.length-1; i >=0  ; i--){
            var id = '';
            var current_text = '';
            var current_class = '';
            if(data[i].id == current_round_id){
                id = "id='r_"+data[i].league.data.current_round_id+"'";
                current_text = "(current)";
                current_class = "current_round_cl";
            }

            var roundContent = '';
            roundContent += "<div class='result_content fl_1 fl_colum'>";
            roundContent += "                        <div class=' "+current_class+" round_title fl_1 fl_row' "+id+" >" +
                "                            <div class='round_name_text'>"+ languages[siteLang].round +" "+data[i].name+" "+current_text+"</div>" +
                "                        </div>";
            for(var j = 0; j < data[i].fixtures.data.length; j++){
                var htContent = "";
                var scoreContent = '';
                var ht_hash = [];
                var liveClass = "";
                if(
                    data[i].fixtures.data[j].time.status == "SUSP" ||
                    data[i].fixtures.data[j].time.status == "POSTP" ||
                    data[i].fixtures.data[j].time.status == "DELAYED" ||
                    data[i].fixtures.data[j].time.status == "TBA" ||
                    data[i].fixtures.data[j].time.status == "CANCL"
                ){
                    if(data[i].fixtures.data[j].scores.ht_score !== null){
                        ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                        htContent +=
                            "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                            "    <div class='round_game_1_team_result'>-</div>" +
                            "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                    }else{
                        htContent += "<div>"+data[i].fixtures.data[j].time.status+"</div>";
                    }
                    scoreContent +=
                        "<div class='round_game_1_team_result'></div>" +
                        "<div class='round_game_1_team_result'>-</div>" +
                        "<div class='round_game_1_team_result'></div>";
                }else if(data[i].fixtures.data[j].time.status == "AWARDED"){
                    htContent += "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].time.status+"</div>";
                    scoreContent +=
                        "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                        "<div class='round_game_1_team_result'>-</div>" +
                        "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
                }else if(data[i].fixtures.data[j].time.status == "HT"){
                    liveClass = "live_match_game_res";
                    if(data[i].fixtures.data[j].scores.ht_score !== null){
                        ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                        htContent +=
                            "    <div class='round_game_1_team_result "+liveClass+"'>"+ht_hash[0]+"</div>" +
                            "    <div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                            "    <div class='round_game_1_team_result  "+liveClass+"'>"+ht_hash[2]+"</div>"
                    }else{
                        htContent += "<div class='live_match_game_res'>"+data[i].fixtures.data[j].time.minute+"'</div>";
                    }
                    scoreContent +=
                        "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                        "<div class='round_game_1_team_result xx "+liveClass+"'>-</div>" +
                        "<div class='round_game_1_team_result xx "+liveClass+"'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
                }else if(data[i].fixtures.data[j].time.status == "NS"){
                    htContent += "<div class=''>"+data[i].fixtures.data[j].time.status+"</div>";
                    scoreContent +=
                        "<div class='round_game_1_team_result'>-</div>";
                }else if(data[i].fixtures.data[j].time.status == "LIVE"){
                    liveClass = "live_match_game_res";
                    htContent += "<div class='live_match_game_res'>"+data[i].fixtures.data[j].time.status+" "+data[i].fixtures.data[j].time.minute+"'</div>";
                    scoreContent +=
                        "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                        "<div class='round_game_1_team_result  "+liveClass+"'>-</div>" +
                        "<div class='round_game_1_team_result  "+liveClass+"'>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";
                }else if(data[i].fixtures.data[j].time.status == "FT"){
                    if(data[i].fixtures.data[j].scores.ht_score !== null){
                        ht_hash = data[i].fixtures.data[j].scores.ht_score.split('');
                        htContent +=
                            "    <div class='round_game_1_team_result'>"+ht_hash[0]+"</div>" +
                            "    <div class='round_game_1_team_result'>-</div>" +
                            "    <div class='round_game_1_team_result'>"+ht_hash[2]+"</div>"
                    }else{
                        htContent += "<div>"+data[i].fixtures.data[j].time.status+"</div>";
                    }
                    scoreContent +=
                        "<div class='round_game_1_team_result  '>"+data[i].fixtures.data[j].scores.localteam_score+"</div>" +
                        "<div class='round_game_1_team_result  '>-</div>" +
                        "<div class='round_game_1_team_result  '>"+data[i].fixtures.data[j].scores.visitorteam_score+"</div>";

                }


                // htContent += "<div class='round_game_1_team_result'>"+data[i].fixtures.data[j].time.status+"</div>";
                roundContent +="   <a href='/match/"+data[i].fixtures.data[j].id+"' class=' round_row fl_1 fl_row'  target='popup'" +
                    " onclick=\"window.open('/football/match/"+data[i].fixtures.data[j].id+"','popup','width=660,height=620'); return false;\">" +
                    // roundContent += "             <div class=' round_row fl_1 fl_row'>" +
                    "                            <div class='round_game_data'>"+data[i].fixtures.data[j].time.starting_at.date_time+"</div>" +
                    "                            <div class='round_game_teams fl_row'>" +
                    // "                                    <a href='/football/team/"+data[i].fixtures.data[j].localTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].fixtures.data[j].localTeam.data.name+"</a>" +
                    "                                    <div  class='round_game_1_team_name fl_1 fl_row_reverse'>"+data[i].fixtures.data[j].localTeam.data.name+"</div>" +
                    scoreContent+
                    // "                                    <a href='/football/team/"+data[i].fixtures.data[j].visitorTeam.data.id+"' class='round_game_1_team_name fl_1 fl_row'>"+data[i].fixtures.data[j].visitorTeam.data.name+"</a>" +
                    "                                    <div  class='round_game_1_team_name fl_1 fl_row'>"+data[i].fixtures.data[j].visitorTeam.data.name+"</div>" +
                    "                            </div>" +
                    "<div class='round_game_ht fl_row'>" +
                    htContent+
                    "</div>"+
                    "                        </a>"
            }
            roundContent += '</div>';
            content.push(roundContent);
        }
    }else{
        content.push('<div class="s_row fl_1  fl_row result_no">There are no Results</div>')
    }


    $('.page_result_data').html(content);
    // var elemRect = $('#r_'+current_round_id+'');
    // var elementPosition =elemRect.position();
    // if(typeof elementPosition !== "undefined"){
    //     var topPx = elementPosition.top;
    //     var body = $("html, body");
    //     body.stop().animate({scrollTop:topPx}, 1000, 'swing', function() {
    //
    //     });
    // }

}

function setStandingsContent(data) {
    var content = [];

    // console.log('data',data)
    if(data[0].resource === "stage") {

        var list = data[0].standings.data;
        // console.log('list', list)
        // if()
        content.push("" +
            "<div class=\"standings_title fl_1 fl_row\">\n" +
            "                            <div class=\"standings_t_num\">#</div>\n" +
            "                            <div class=\"standings_t_name fl_1 fl_row\">"+languages[siteLang].standing_team+"</div>\n" +
            "                            <div class=\"standings_t_mp\">MP</div>\n" +
            "                            <div class=\"standings_t_w\">W</div>\n" +
            "                            <div class=\"standings_t_d\">D</div>\n" +
            "                            <div class=\"standings_t_l\">L</div>\n" +
            "                            <div class=\"standings_t_g\">G</div>\n" +
            "                            <div class=\"standings_t_pts\">"+languages[siteLang].standing_pts+"</div>\n" +
            "                            <div class=\"standings_t_form\">"+languages[siteLang].standing_form+"</div>\n" +
            "                        </div>");
        for (var i = 0; i < list.length; i++) {
            var positionColorClass = "";
            if (list[i].position === 1 || list[i].position === 2 || list[i].position === 3 || list[i].position === 4) {
                positionColorClass = "num_color_blue";
            } else if (list[i].position === 5 || list[i].position === 6) {
                positionColorClass = "num_color_darkred";
            } else if (list[i].position === 7) {
                positionColorClass = "num_color_pink";
            } else if (list[i].position === 18 || list[i].position === 19 || list[i].position === 20) {
                positionColorClass = "num_color_red";
            }
            let gameResCont = "";
            let game_results = list[i].recent_form.split("");
            for (let j = 0; j < game_results.length; j++) {
                let colorClass = "";
                if (game_results[j] === "W") {
                    colorClass = "item_win";
                } else if (game_results[j] === "D") {
                    colorClass = "item_no_one";
                } else {
                    colorClass = "item_loose";
                }
                gameResCont += "<div class='standings_form_item " + colorClass + "'><span class='mob_none'>" + game_results[j] + "</span></div>";
            }

            content.push(
                " <div class='standings_row fl_1 fl_row'>" +
                "    <div class='standings_num " + positionColorClass + "' title='" + list[i].result + "'>" + list[i].position + ".</div>" +
                "    <a href='/football/team/" + list[i].team_id + "' class='standings_name fl_1 fl_row'>" +
                // "         <img src='/images/test/real-madrid-c-f-logo-png-transparent.png' alt='asd' />" +
                list[i].team_name +
                "    </a>" +
                "    <div class='standings_mp'>" + list[i].overall.games_played + "</div>" +
                "    <div class='standings_w'>" + list[i].overall.won + "</div>" +
                "    <div class='standings_d'>" + list[i].overall.draw + "</div>" +
                "    <div class='standings_l'>" + list[i].overall.lost + "</div>" +
                "    <div class='standings_g'>" + list[i].overall.goals_scored + " : " + list[i].overall.goals_against + "</div>" +
                "    <div class='standings_pts'>" + list[i].points + "</div>" +
                "    <div class='standings_form fl_row'>" +
                gameResCont +
                "     </div>" +
                " </div>"
            )
        }


        content.push("<div class=\"promotion_colors fl_1 fl_colum\">\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_blue\"></div>" + languages[siteLang].promotion +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_darkred\"></div>" + languages[siteLang].promotion +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_pink\"></div>" + languages[siteLang].promotion +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_red\"></div>" + languages[siteLang].relegation +
            "                    </div>\n" +
            "                </div>\n" +
            "                <div class=\"end_page_text\">\n" +
            languages[siteLang].stands_bottom_text +
            "                </div>");
    }else if(data[0].resource === "group"){

        for(var l = 0; l < data.length; l++){
            var list = data[l].standings.data;
            // console.log('list', list)
            // if()
            content.push("<div class='group_stage_gr_title'>"+data[l].name+"</div>")
            content.push("" +
                "<div class=\"standings_title fl_1 fl_row\">\n" +
                "                            <div class=\"standings_t_num\">#</div>\n" +
                "                            <div class=\"standings_t_name fl_1 fl_row\">Team</div>\n" +
                "                            <div class=\"standings_t_mp\">MP</div>\n" +
                "                            <div class=\"standings_t_w\">W</div>\n" +
                "                            <div class=\"standings_t_d\">D</div>\n" +
                "                            <div class=\"standings_t_l\">L</div>\n" +
                "                            <div class=\"standings_t_g\">G</div>\n" +
                "                            <div class=\"standings_t_pts\">Pts</div>\n" +
                "                            <div class=\"standings_t_form\">Form</div>\n" +
                "                        </div>");
            for (var i = 0; i < list.length; i++) {
                var positionColorClass = "";
                if (list[i].position === 1 || list[i].position === 2) {
                    positionColorClass = "num_color_blue";
                } else if (list[i].position === 3) {
                    positionColorClass = "num_color_darkred";
                }
                let gameResCont = "";
                let game_results = list[i].recent_form.split("");
                for (let j = 0; j < game_results.length; j++) {
                    let colorClass = "";
                    if (game_results[j] === "W") {
                        colorClass = "item_win";
                    } else if (game_results[j] === "D") {
                        colorClass = "item_no_one";
                    } else {
                        colorClass = "item_loose";
                    }
                    gameResCont += "<div class='standings_form_item " + colorClass + "'><span class='mob_none'>" + game_results[j] + "</span></div>";
                }

                content.push(
                    " <div class='standings_row fl_1 fl_row'>" +
                    "    <div class='standings_num " + positionColorClass + "' title='" + list[i].result + "'>" + list[i].position + ".</div>" +
                    "    <a href='/football/team/" + list[i].team_id + "' class='standings_name fl_1 fl_row'>" +
                    // "         <img src='/images/test/real-madrid-c-f-logo-png-transparent.png' alt='asd' />" +
                    list[i].team_name +
                    "    </a>" +
                    "    <div class='standings_mp'>" + list[i].overall.games_played + "</div>" +
                    "    <div class='standings_w'>" + list[i].overall.won + "</div>" +
                    "    <div class='standings_d'>" + list[i].overall.draw + "</div>" +
                    "    <div class='standings_l'>" + list[i].overall.lost + "</div>" +
                    "    <div class='standings_g'>" + list[i].overall.goals_scored + " : " + list[i].overall.goals_against + "</div>" +
                    "    <div class='standings_pts'>" + list[i].points + "</div>" +
                    "    <div class='standings_form fl_row'>" +
                    gameResCont +
                    "     </div>" +
                    " </div>"
                )
            }



        }
        content.push("<div class=\"promotion_colors fl_1 fl_colum\">\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_blue\"></div> Promotion - Champions League (Play Offs)\n" +
            "                    </div>\n" +
            "                    <div class=\"promotion_color fl_1 fl_row\">\n" +
            "                        <div class=\"promotion_qarg_f num_color_darkred\"></div> Promotion - Europa League (Play Offs)\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "                <div class=\"end_page_text\">\n" +
            "                    If teams finish on equal points at the end of the season, head-to-head matches will be the tie-breaker.\n" +
            "                </div>");
    }
        $('.page_result_data').html(content);
}



$('.like_star').click(function () {
    var leagueId = $('.liga_name_s').attr('data-league-id');
    var url = '/football/league/add-like'
    requestPost(url,'leagueId='+leagueId,function () {
        // if(this.)
        if(this.readyState == 4){
            var result = JSON.parse(this.responseText);
            if(!result.error){
                window.location.reload()
            }else{
                //console.log('error')
            }
        }
    })
})
