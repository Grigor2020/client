    var pageAllGames = 1;
    var pageDayGames = 1;
    var mData = [];
    var dayData = [];
    var liveData = [];
    var selectedDay = null;

    function loadDataForOneDay(day) {
        if(selectedDay == day){

        }else {
            pageDayGames = 1;
            dayData = []
        }
        if(mData.length === 0){
            return false
        }else {
            $('.games_by_liga').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
            var url = '/getGamesPerDay'
            requestPost(url,'page='+pageDayGames+'&day='+day,function () {
                // if(this.)
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    var pageStatus = true;
                    var moreAction = ''
                    //console.log('result',result)
                    if(!result.error){
                        moreAction = 'dayGames';
                        selectedDay = day;
                        if(result.data.length === 0){
                            pageStatus = false
                        }
                        if (dayData.length === 0) {
                            dayData = result.data;
                        } else {
                            dayData = dayData.concat(result.data)
                        }
                        // console.log('mData',mData)
                        // var topLeagues = result.topLeagues;
                        $('.game_type').removeClass('active_game_type');
                        pageDayGames++;
                        setAllGamesContent(dayData,pageStatus,moreAction);
                    }else{
                        setAllGamesContent([],pageStatus,moreAction);
                    }
                }
            })
        }
    }

    $(document).ready(function () {
        loadLiveGames();
        $(document).on('click','.load_more_button', function () {
            var type = $(this).attr('data-action')
            if(type == '1'){
                loadData();
            }else if(type == '2'){
                loadDataForOneDay(selectedDay)
            }
            return false;
        })

        function loadData() {
            $('.games_by_liga').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
            var url = '/getLivescores'
            requestPost(url,'page='+pageAllGames,function () {
                // if(this.)
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    var pageStatus = true;
                    var moreAction = ''
                    if(!result.error){
                        moreAction = 'allData'

                        if(result.data.length === 0){
                            pageStatus = false
                        }
                        if (mData.length === 0) {
                            mData = result.data;
                        } else {
                            mData = mData.concat(result.data)
                        }
                        // console.log('mData',mData)
                        // var topLeagues = result.topLeagues;
                        pageAllGames++;
                        setAllGamesContent(mData,pageStatus,moreAction);
                    }else{
                        setAllGamesContent([],pageStatus,moreAction);
                    }
                }
            })
        }

        function loadFinishedGames() {
            $('.games_by_liga').html("<img class='loading_main' src='/images/content/loading-png-gif.gif' alt='loading'>");
            var url = '/getLivescores'
            requestPost(url,'page='+pageAllGames,function () {
                // if(this.)
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(!result.error){
                        if(mData.length === 0){
                            mData = result.data;
                        }else {
                            mData = mData.concat(result.data)
                        }
                        // console.log('mData',mData)
                        // var topLeagues = result.topLeagues;
                        pageAllGames++;
                        setFinishedGamesContent(mData);
                    }else{
                        setFinishedGamesContent([]);
                    }
                }
            })
        }




        function loadLiveGames(){
            var url = '/getLivescoresNow'
            requestPost(url,'',function () {
                // if(this.)
                if(this.readyState == 4){
                    var result = JSON.parse(this.responseText);
                    if(!result.error){
                        liveData = result.data;
                        setLiveGamesContent(liveData)
                    }else{
                        setLiveGamesContent([])
                    }
                }
            })
            // let liveLeagues = [];
            // for(var i = 0; i < data.length; i++){
            //     for(var j = 0; j < data[i].length; j++){
            //         if(data[i][j].mainData.time.status === "LIVE" || data[i][j].mainData.time.status === "HT"){
            //             liveLeagues.push(data[i]);
            //         }
            //     }
            //     // break
            // }
            // console.log('liveLeagues',liveLeagues)
            // setLiveGamesContent(liveLeagues);

        }


        $('.game_type').click(function () {
            var checkType = $('.active_game_type').attr('data-type');
            var type =  $(this).attr('data-type');
            if(checkType != type){
                $('.game_type').removeClass('active_game_type');
                $(this).addClass('active_game_type');
                if(type === '1'){
                    loadData();
                }
                if(type === '2'){
                    loadLiveGames();
                }
                if(type === '4'){
                    loadFinishedGames();
                }
            }else{
                return false;
            }
        })

        function setFinishedGamesContent(data){
            var content = [];
            for(var z = 0; z < data.length; z++) {
                for (let x = 0; x < data[z].length; x++) {
                    if(data[z][x].time.status !== "FT"){
                        data[z].splice(x,1)
                    }
                }
            }
            for(var b = 0; b < data.length; b++) {
                if(data[b].length === 0){
                    data[b].splice(b,1)
                }
            }
            for(var i = 0; i < data.length; i++){
                for(let j = 0; j < data[i].length; j++){
                    if(j === 0){
                        var img = "";
                        if(data[i][0].league.data.logo_path){
                            img += "" +
                                "<div class='g_liga_flag'>" +
                                "       <img src='"+data[i][0].league.data.logo_path+"' alt='flag' />" +
                                "    </div>";
                        }
                        // "                    <div class='check_l_name'>" +
                        // "                    </div>" +
                        // "                        <div class='check_l_box'></div>" +
                        content.push("" +
                            "<div class='g_l_title'>" +
                            img+
                            "                    <div class='g_liga_names'>" +
                            // "                        <span class='g_country_name'>"+data[i][0].league.data.name+": </span>" +
                            "                        <span class='g_liga_name' title='"+data[i][0].league.data.name+"'>"+data[i][0].league.data.name+"</span>" +
                            "                    </div>" +
                            "                    <div class='g_liga_standings'>"+languages[siteLang]['standings']+"</div>" +
                            // "                    <div class='g_l_action'><img src='/images/content/angle-up-solid.svg' alt='a' /></div>" +
                            "                </div>")
                    }
                    let time = "";
                    if(!data[i][j].time.minute){
                        time +="<div class='g_c_time'><span class='g_c_extime'>"+data[i][j].time.starting_at.date_time+"</span></div>"
                    }else{
                        time +="<div class='g_c_time'><span class='g_c_intime'>"+data[i][j].time.minute+"'</span></div>"
                    }
                    let hashClass = data[i][j].time.status === 'LIVE' || data[i][j].time.status === "HT" ? 'g_c_num_detail_live' : 'g_c_num_detail_notlive'
                    let gameStatus = "";
                    if(data[i][j].time.status === "LIVE" || data[i][j].time.status === "HT"){
                        gameStatus+= "<div class='live_bet_button'><span>"+data[i][j].time.status+"</span></div>";
                    }else{
                        gameStatus+= "<div class='prev_live'><span>"+data[i][j].time.status+"</span></div>";
                    }
                    content.push("" +
                        "<div class='g_l_content fl_row'>" +
                        time+
                        "   <a href='/match/"+data[i][j].id+"' class='g_c_names  fl_row'  target='popup'" +
                        " onclick=\"window.open('/football/match/"+data[i][j].id+"','popup','width=660,height=620'); return false;\">" +
                        "       <div class='g_c_name_1 fl_row_reverse'>"+data[i][j].localTeam.data.name+"</div>" +
                        "       <div class='"+hashClass+" fl_row'>" +
                        "           <div class='fl_1 '>"+data[i][j].scores.localteam_score+"</div>" +
                        "           <div class=''> - </div>" +
                        "           <div class='fl_1'>"+data[i][j].scores.visitorteam_score+"</div>" +
                        "       </div>" +
                        "       <div class='g_c_name_2 fl_row'>"+data[i][j].visitorTeam.data.name+"</div>" +
                        "   </a>" +
                        // "   <div class='g_c_chanals'>" +
                        // "       <img src='/images/content/tv-solid.svg' alt='chanals' />" +
                        // "   </div> " +
                        // "   <div class='g_c_lineups'>" +
                        // "       <img src='/images/content/tshirt-solid.svg' alt='tshirt' />" +
                        // "   </div>" +
                        gameStatus+
                        "</div>")
                }

            }

            // content.push("<div class='load_more_button'>More</div>");
            $('.games_by_liga').html(content);
        }



        function setLiveGamesContent(data) {
            //console.log('XXXXXXXXXXX',data)
            if(data.length === 0){
                $('.sport_num').css({"display":"none"})
                $('.games_by_liga').html("<div class=\"no_match_text\">No match found.</div>")
            }else {
                var liveGamesCount = 0;
                var likeLeaguesIds = [];
                for (var i = 0; i < likeLeagues.length; i++) {
                    likeLeaguesIds.push(likeLeagues[i].sportmonks_id)
                }
                $('.row_menu_lives').css({"display": "none"});
                var content = [];
                for (var i = 0; i < data.length; i++) {

                    for (let j = 0; j < data[i].length; j++) {
                        if (j === 0) {

                            var img = "";
                            if (data[i][0].league.data.logo_path) {

                                img += "" +
                                    "<div class='g_liga_flag'>" +
                                    "       <img src='" + data[i][0].league.data.logo_path + "' alt='flag' />" +
                                    "    </div>";
                            }
                            content.push("" +
                                "<div class='g_l_title'>" +
                                img +
                                "                    <a href='/football/league/" + data[i][0].league_id + "' class='g_liga_names'>" +
                                // "                        <span class='g_country_name'>"+data[i][0].league.data.name+": </span>" +
                                "                        <span class='g_liga_name' title='" + data[i][0].league.data.name + "'>" + data[i][0].league.data.name + "</span>" +
                                "                    </a>" +
                                "                    <div class='g_liga_standings'>Standings</div>" +
                                // "                    <div class='g_l_action'><img src='/images/content/angle-up-solid.svg' alt='a' /></div>" +
                                "                </div>")
                        }
                        if (likeLeaguesIds.indexOf(data[i][j].league.data.id) !== -1) {
                            $('#row-live-' + data[i][j].league.data.id + '').css({"display": "flex"})
                        }
                        let time = "";
                        if (!data[i][j].time.minute) {
                            time += "<div class='g_c_time'><span class='g_c_extime'>" + data[i][j].time.starting_at.date_time + "</span></div>"
                        } else {
                            time += "<div class='g_c_time'><span class='g_c_intime'>" + data[i][j].time.minute + "'</span></div>"
                        }
                        let hashClass = data[i][j].time.status === 'LIVE' || data[i][j].time.status === "HT" ? 'g_c_num_detail_live' : 'g_c_num_detail_notlive'
                        let gameStatus = "";
                        if (data[i][j].time.status === "LIVE" || data[i][j].time.status === "HT") {
                            gameStatus += "<div class='live_bet_button'><span>" + data[i][j].time.status + "</span></div>";
                        } else {
                            gameStatus += "<div class='prev_live'><span>" + data[i][j].time.status + "</span></div>";
                        }
                        content.push("" +
                            "<div class='g_l_content fl_row'>" +
                            time +
                            "   <a href='/match/" + data[i][j].id + "' class='g_c_names  fl_row'  target='popup'" +
                            " onclick=\"window.open('/football/match/" + data[i][j].id + "','popup','width=660,height=620'); return false;\">" +
                            "       <div class='g_c_name_1 fl_row_reverse'>" + data[i][j].localTeam.data.name + "</div>" +
                            "       <div class='" + hashClass + " fl_row'>" +
                            "           <div class='fl_1 '>" + data[i][j].scores.localteam_score + "</div>" +
                            "           <div class=''> - </div>" +
                            "           <div class='fl_1'>" + data[i][j].scores.visitorteam_score + "</div>" +
                            "       </div>" +
                            "       <div class='g_c_name_2 fl_row'>" + data[i][j].visitorTeam.data.name + "</div>" +
                            "   </a>" +
                            // "   <div class='g_c_chanals'>" +
                            // "       <img src='/images/content/tv-solid.svg' alt='chanals' />" +
                            // "   </div> " +
                            // "   <div class='g_c_lineups'>" +
                            // "       <img src='/images/content/tshirt-solid.svg' alt='tshirt' />" +
                            // "   </div>" +
                            gameStatus +
                            "</div>")
                        liveGamesCount++;
                    }

                }
                $('.games_by_liga').html(content)
                $('.sport_num').css({"display":"flex"})
                $('.sport_num').text(liveGamesCount)
            }
        }



    })


    function setAllGamesContent(data,page,moreAction) {
        var content = [];
        // console.log('data',data)

        // console.log('setAllGamesContent',data)

        for (var i = 0; i < data.length; i++) {

            for (let j = 0; j < data[i].length; j++) {
                if (typeof data[i][j].visitorTeam === "undefined" || typeof data[i][j].localTeam === "undefined") {
                    continue
                }
                if (j === 0) {
                    var img = "";
                    if (data[i][0].league.data.logo_path) {
                        img += "" +
                            "<div class='g_liga_flag'>" +
                            "       <img src='" + data[i][0].league.data.logo_path + "' alt='flag' />" +
                            "    </div>";
                    }
                    content.push("" +
                        "<div class='g_l_title'>" +
                        img +
                        "                    <a href='/football/league/" + data[i][0].league_id + "' class='g_liga_names'>" +
                        // "                        <span class='g_country_name'>"+data[i][0].league.data.name+": </span>" +
                        "                        <span class='g_liga_name' title='" + data[i][0].league.data.name + "'>" + data[i][0].league.data.name + "</span>" +
                        "                    </a>" +
                        "   <a href='/football/league/standing/" + data[i][0].league_id + "' class='g_liga_standings  fl_row'  target='popup'" +
                        " onclick=\"window.open('/football/league/standing/" + data[i][0].league_id + "','popup','width=660,height=620'); return false;\">" +
                        "                    "+languages[siteLang]['standings']+"</a>" +
                        // "                    <div class='g_l_action'><img src='/images/content/angle-up-solid.svg' alt='a' /></div>" +
                        "                </div>")
                }
                let time = "";
                if(data[i][j].time.status === "FT"){
                    time = '<div class="g_c_time">Finished</div>'
                }else{
                    if (!data[i][j].time.minute) {
                        time += "<div class='g_c_time'><span class='g_c_extime'>" + data[i][j].time.starting_at.date_time + "</span></div>"
                    } else {
                        time += "<div class='g_c_time'><span class='g_c_intime'>" + data[i][j].time.minute + "'</span></div>"
                    }
                }

                let hashClass = data[i][j].time.status === 'LIVE' || data[i][j].time.status === "HT" ? 'g_c_num_detail_live' : 'g_c_num_detail_notlive'
                let gameStatus = "";
                if (data[i][j].time.status === "LIVE" || data[i][j].time.status === "HT") {
                    gameStatus += "<div class='live_bet_button'><span>" + data[i][j].time.status + "</span></div>";
                } else {
                    gameStatus += "<div class='prev_live'><span>" + data[i][j].time.status + "</span></div>";
                }
                content.push("" +
                    "<div class='g_l_content fl_row'>" +
                    time +
                    "   <a href='/match/" + data[i][j].id + "' class='g_c_names  fl_row'  target='popup'" +
                    " onclick=\"window.open('/football/match/" + data[i][j].id + "','popup','width=660,height=620'); return false;\">" +
                    "       <div class='g_c_name_1 fl_row_reverse'>" + data[i][j].localTeam.data.name + "</div>" +
                    "       <div class='" + hashClass + " fl_row'>" +
                    "           <div class='fl_1 '>" + data[i][j].scores.localteam_score + "</div>" +
                    "           <div class=''> - </div>" +
                    "           <div class='fl_1'>" + data[i][j].scores.visitorteam_score + "</div>" +
                    "       </div>" +
                    "       <div class='g_c_name_2 fl_row'>" + data[i][j].visitorTeam.data.name + "</div>" +
                    "   </a>" +
                    // "   <div class='g_c_chanals'>" +
                    // "       <img src='/images/content/tv-solid.svg' alt='chanals' />" +
                    // "   </div> " +
                    // "   <div class='g_c_lineups'>" +
                    // "       <img src='/images/content/tshirt-solid.svg' alt='tshirt' />" +
                    // "   </div>" +
                    gameStatus +
                    "</div>")
            }

        }
        var moreActions = '';
        if(moreAction == 'dayGames'){
            moreActions = 2;
        }
        if(moreAction == 'allData'){
            moreActions = 1;
        }
        if(page){
            content.push("<div class='load_more_button' data-action='"+moreActions+"'>More</div>");
        }


        $('.games_by_liga').html(content);
    }



