
$(document).ready(function () {
    /* for lang */
    $(document).on("change",".onoffswitch-checkbox",function() {
        if($(this).prop("checked")){
            $('.forrus').css('display','none');
            $('.foreng').css('display','block');
        }else{
            $('.forrus').css('display','block');
            $('.foreng').css('display','none');
        }
    });

    $(".lang_colums").click(function () {
        $(".lang_colums").removeClass('active');
        $(this).addClass('active');
        $('.foren').css({'display':'none'})
        $('.forru').css({'display':'none'})
        $('.foram').css({'display':'none'})
        $('.for'+$(this).attr('data-name')+'').css({
            'display':'block'
        })
    });

    $("._radio_lang").change(function() {
        var langIndex = $(this).attr("data-name");
        $("._change_lang").each(function () {
            if(langIndex==$(this).attr("data-lnag")){
                $(this).css('display','block');
            }else{
                $(this).css('display','none');
            }
        })
    });
    /* for lang end */

    /* cookie notification */
    var cok = getCookie('not');
    if(cok == 'up'){
        $('#update_not').css('display','block');
        $('#save_not').css('display','none');
        $('.save_done').css('top','0');
        deleteCookie('not');
        setTimeout(function(){
            $('.save_done').css('top','-120px');
        },2000)
    }

    if(cok == 'save'){
        $('#save_not').css('display','block');
        $('#update_not').css('display','none');
        $('.save_done').css('top','0');
        deleteCookie('not');
        setTimeout(function(){
            $('.save_done').css('top','-120px');
        },2000)
    }
    /* cookie notification */

    $('.setsize').click(function () {
        elementChild = $(this).children('i');
        elementChild.toggleClass('fa-minus');
        elementChild.toggleClass('fa-plus');
        element = $(this).parent('div').parent('div').next('div.box_edit');
        element.slideToggle();
    });

    $( "._foto_block" ).click(function() {
      $(this).children(".img_file" )[0].click();
    });
    $( ".foto_block img" ).click(function() {
        $(this).next(".img_file" )[0].click();
    });

    $( "._chose_file" ).click(function() {
        $(this).next('.foto_block').children(".img_file" )[0].click();
    });


    function renderImage(file,x){
        var reader = new FileReader();
        reader.onload = function(event){
                the_url = event.target.result;
                $(x).prev('img').css('display','block');
                $(x).prev('img').attr('src',the_url);
        }
        reader.readAsDataURL(file);
    }

    $( ".img_file" ).change(function() {
        $(this).parent('div.foto_block').removeClass('forempty');
        $(this).parent('div.foto_block').addClass('forfull');
        renderImage(this.files[0],this)
    });

    /*********** Absolute img ************/
        // $( "._foto_block" ).click(function() {
        //     $(this).children(".img_file" )[0].click();
        // });


        function renderImageAb(file,x){
            var reader = new FileReader();
            reader.onload = function(event){
                the_url = event.target.result;
                $(x).prev('img').css('display','block');
                $(x).prev('img').attr('src',the_url);
            }
            reader.readAsDataURL(file);
        }

        $( ".ab_img_f" ).change(function() {
            $(this).parent('div.foto_block').removeClass('forempty');
            $(this).parent('div.foto_block').addClass('forfull');
            renderImageAb(this.files[0],this)
        });
    /***********************/

    /****************************************/
        // $( "._video_block" ).click(function() {
        //     $(this).children(".video_file" )[0].click();
        // });


        $( "._chose_video" ).click(function() {
            $(this).next('.video_block').children(".video_file" )[0].click();
            //console.log('asd')
        });

        function renderVideo(file,x){
            var reader = new FileReader();
            reader.onload = function(event){
                the_url = event.target.result;
                $(x).next('video').css('display','block');
                $(x).next('video').attr('src',the_url);
                //$(x).prev('video').children('source').attr('src',the_url);
            }
            reader.readAsDataURL(file);
        }

        $( ".video_file" ).change(function() {
            $(this).parent('div.video_block').removeClass('forempty');
            $(this).parent('div.video_block').addClass('forfull');
            renderVideo(this.files[0],this)
        });
    /****************************************/

    //$(".action_delete").click(function(){
    $(document).on("click",".action_delete",function() {
        if(!confirm("Do you really want to delete?")){return false;}
        var self = $(this);

        var datGet = $(this).data('get');
        var datPage = $(this).data('page');
        var url = "";
        if(typeof datPage != "undefined" && datPage != ""){
            url = "/"+datGet+"/"+datPage+"/delete/";
        }else{
            url = "/"+datGet+"/delete/";
        }

        var id = $(this).data('id');
        var body = "id="+id+"";
        requestPost(url,body,function(){
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    self.parent('td').parent('tr').fadeOut();
                }else{

                }
            }
        })
    });

    // function openSub(e){
    //     $('.menu_b_row_content').removeClass('active_menu_b_row_content');
    //     $('.menu_cilds').removeClass('active_menu_cilds');
    //     console.log('e',e)
    // }

    // $('.menu_b_row').click(function () {
    //     $('.menu_b_row_content').removeClass('active_menu_b_row_content');
    //     $('.menu_cilds').removeClass('active_menu_cilds');
    //     var allrows = $('.menu_b_row');
    //     var rowsWithChild = [];
    //     for(var i = 0; i < allrows.length; i++){
    //         if($(allrows[i]).find('div.menu_cilds').length !== 0){
    //             rowsWithChild.push($(allrows[i]));
    //         }
    //     }
    //     console.log('rowsWithChild',rowsWithChild)
    // })

    $('.h_username').click(function () {
        if($(this).next('.active_h_userlist').length > 0){
            $(this).next('.h_userlist').removeClass('active_h_userlist');
        }else{
            $('.h_userlist').removeClass('active_h_userlist');
            $(this).next('.h_userlist').addClass('active_h_userlist');
        }
    });

});

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function deleteCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
}

function setCook(name,value){
    document.cookie = name + "=" +value;
}

function requestPost(url,body,callback){
    var oAjaxReq = new XMLHttpRequest();
    oAjaxReq.open("post", url, true);
    oAjaxReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    oAjaxReq.send(body);
    oAjaxReq.onreadystatechange = callback;
}
