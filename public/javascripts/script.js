$(document).ready(function () {
    $('.__on_login').click(function () {
        $('.reg_popup').css({'display' : 'none'});
        $('.login_popup').css({'display' : 'flex'});
        $('.abs_layer').css({'display' : 'block'});
    })
    $('.__on_reg').click(function () {
        $('.login_popup').css({'display' : 'none'});
        $('.reg_popup').css({'display' : 'flex'});
        $('.abs_layer').css({'display' : 'block'});

    })
    $('.__on_change_password').click(function () {
        $('.change_pass_popup').css({'display' : 'flex'});
        $('.abs_layer').css({'display' : 'block'});
    })
    $('.__on_open_menu').click(function () {
        $('.menu_popup').css({'display' : 'flex'});
    })
    $('.close_menu_popup').click(function () {
        $('.menu_popup').css({'display' : 'none'});
    })
    $('.abs_layer').click(function () {
        $('.login_popup').css({'display' : 'none'});
        $('.reg_popup').css({'display' : 'none'});
        $('.abs_layer').css({'display' : 'none'});
        $('.change_pass_popup').css({'display' : 'none'});
    })
    $('.change_lang').click(function () {
        if($(this).hasClass('active_lang')){
            return false
        }else {
            var lang = $(this).attr('data-lang');
            document.cookie = "lang="+lang+"; expires=Thu, 18 Dec 2030 12:00:00 UTC; path=/";
            window.location.href = "/";
        }
    })


    $('.countries_more').click(function () {
        $('.countries_main').css({"height" : "auto"});
        $(this).css({"display":"none"})
    })

    $('.__log_us').click(function () {
        $('.login_menu').toggleClass('login_menu_active');
    })

    $('.del_leg').click(function () {
        var self = $(this);
        var leagueId = $(this).attr('data-lid');
        var url = '/football/league/del-like';
        requestPost(url,'leagueId='+leagueId,function () {
            // if(this.)
            if(this.readyState == 4){
                var result = JSON.parse(this.responseText);
                if(!result.error){
                    window.location.reload()
                }else{
                    //console.log('error')
                }
            }
        })
    })
})

$(document).on('click','.calendar-header',function () {
    // var heigth = document.getElementById('pnlSimpleCalendar').style.height;
    // console.log('heigth',heigth)
    // if(heigth == 217){
    //     $('#pnlSimpleCalendar').css({"height":"35px"})
    // }
    // if(heigth == 35){
    //     $('#pnlSimpleCalendar').css({"height":"217px"})
    // }

})
// $('.calendar-header').click(function () {
//
// })


// $(document).mousedown(function(e)
// {

    // var login_popup = $(".login_popup");
    // if (!login_popup.is(e.target) && login_popup.has(e.target).length === 0) {
    //     $('.login_popup').css({'display' : 'none'});
    //     $('.abs_layer').css({'display' : 'none'});
    //     return false;
    // }
    // var reg_popup = $(".reg_popup");
    // if (!reg_popup.is(e.target) && reg_popup.has(e.target).length === 0) {
    //     $('.reg_popup').css({'display' : 'none'});
    //     $('.abs_layer').css({'display' : 'none'});
    //     return false;
    // }
    // var change_pass_popup = $(".change_pass_popup");
    // if (!change_pass_popup.is(e.target) && change_pass_popup.has(e.target).length === 0) {
    //     $('.change_pass_popup').css({'display' : 'none'});
    //     $('.abs_layer').css({'display' : 'none'});
    //     return false;
    // }
    // var login_menu = $(".login_menu");
    // if (!login_menu.is(e.target) && login_menu.has(e.target).length === 0) {
    //     $('.login_menu').removeClass('login_menu_active');
    // }
    // var all_countries = $(".all_countries");
    // if (!all_countries.is(e.target) && all_countries.has(e.target).length === 0) {
    //     $('.all_countries').css({'display' : 'none'});
    // }

// });

$('.cat_country_name').click(function () {
    $('.cat_this_leagues').css({"display":"none"})
    $(this).parent('.cat_row_main').next('.cat_this_leagues').css({"display":"flex"})
})

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
