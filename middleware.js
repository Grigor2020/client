const request = require('request');
var statics = require('./static');
var topLeagues = require('./topLeagues');
var satelize = require('satelize');


const getTimeInfo = function (req, res, next) {
    var ip = req.ip;
    // console.log('ip')
    req.timezone = "Asia/Yerevan"
    // satelize.satelize({ip:ip}, function(err, payload) {
    //     if(err === null){
    //         req.timezone = payload.timezone
    //     }else {
    //         req.timezone = "Asia/Yerevan"
    //     }
        next();
    // });
}


const checkUser = function (req, res, next) {
    var cookie = req.cookies['fin'];
    if(typeof cookie === 'undefined'){
        req.likeLeagues = topLeagues.topLeagues;
        req.likeTeams = [];
        next();
    }else{
        const formData = {
            "token" :cookie
        };
        request.post(
            {
                url: statics.API_URL+'/auth/check-user',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.cookie('fin', jsonBody.user.token, {expire: 400000000000000 + Date.now()});
                    req.likeLeagues = jsonBody.likes.leagues;
                    req.likeTeams = jsonBody.likes.teams;
                    req.userInfo = jsonBody.user;
                }
                next();
            }
        )
    }
}

// const getTopLeagues = function (req, res, next) {
//     request.post(
//         {
//             url: statics.API_URL+'/football/leagues/topLeagues',
//             headers: {
//                 'Accept': 'application/json',
//                 'authorization' : statics.API_AUTH
//             }
//         }, function (err, httpResponse, body) {
//             var jsonBody = JSON.parse(body);
//             if(!jsonBody.error){
//                 req.topLeagues = jsonBody.data;
//             }
//             next();
//         }
//     )
// }

const setlanguage = function (req, res, next) {
    var cookieLang = req.cookies['lang'];
    if(typeof cookieLang === 'undefined'){
        // res.cookie('lang', 'en');
        // document.cookie = "lang=en; expires=Thu, 18 Dec 2030 12:00:00 UTC; path=/";
        res.cookie('lang', 'en', {expire: 400000000000000 + Date.now()});
        req.lang = 'en';
    }else{
        req.lang = cookieLang;
    }
    next();
}

const getCountries = function (req, res, next) {
    request.post(
        {
            url: statics.API_URL+'/football/countries/all',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            }
        }, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){

                req.countries = jsonBody.data;
            }else {
                req.countries = [];
            }
            next();
        }
    )
}


module.exports.checkUser = checkUser;
module.exports.getCountries = getCountries;
module.exports.setlanguage = setlanguage;
// module.exports.getTopLeagues = getTopLeagues;
module.exports.getTimeInfo = getTimeInfo;
