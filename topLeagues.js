var topLeagues = [
    {
        id:64,
        sportmonks_id : 2,
        name : "Champions League",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/2.png",
        s_country_id : 41
    },
    {
        id:64,
        sportmonks_id : 5,
        name : "Europa League",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/5.png",
        s_country_id : 41
    },
    {
        id:66,
        sportmonks_id : 8,
        name : "Premier League",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/8/8.png",
        s_country_id : 462
    },
    {
        id:99,
        sportmonks_id : 82,
        name : "Bundesliga",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/82.png",
        s_country_id : 11
    },
    {
        id:200,
        sportmonks_id : 384,
        name : "Serie A",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/384.png",
        s_country_id : 251
    },
    {
        id:257,
        sportmonks_id : 564,
        name : "La Liga",
        logo_path :"https://cdn.sportmonks.com/images/soccer/leagues/564.png",
        s_country_id : 32
    }
];


module.exports.topLeagues = topLeagues;
