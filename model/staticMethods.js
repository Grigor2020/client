var checkPermissions = function(pagePermissions,permissions){
    return new Promise(function (resolve) {
        // console.log('pagePermissions',pagePermissions)
        // console.log('permissions',permissions)
        let selfPermissions = [];

        for (let i = 0; i < permissions.length; i++) {
            if (permissions[i]['permissions_id'] == pagePermissions) {
                selfPermissions.push(permissions[i]);
                break;
            }
        }
        let access = true;
        // console.log('selfPermissions[0]',selfPermissions)
        if (selfPermissions[0].is_open == '0') {
            access = false;
        }
        let retObj = {
            access: access,
            readOrWrite: selfPermissions[0].read_or_write
        };
        // console.log('retObj', retObj)
        // console.log('............',retObj)
        resolve(retObj)
    });
}


var getDateInUnix = function(date,time){
    var sDate = date.split('-');
    var tari = parseInt(sDate[0]);
    var amis = parseInt(sDate[1]);
    var or = parseInt(sDate[2]);

    var sTime = time.split(":");
    var jam = parseInt(sTime[0]);
    var rope = parseInt(sTime[1]);

    var datum = new Date(Date.UTC(tari,amis-1,or,jam,rope,0));
    return datum.getTime()/1000;
};

function generateUrlFrmoString(string) {
    const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
    const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
}

module.exports.checkPermissions = checkPermissions;
module.exports.getDateInUnix = getDateInUnix;
module.exports.generateUrlFrmoString = generateUrlFrmoString;
