const db = require('../connection');
var log = require("../../logs/log");

var adminLogin = function (params,cb) {
    let prepareSql = "SELECT * FROM places_users WHERE `log` = '"+ params.login +"' AND `pass` = '"+ params.password +"'";
    db.query(prepareSql, function (err, rows) {
        if (err) throw err;
        cb(rows)
    })
};

/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAll = function(table,order,cb){
    let prepareSql = '';
    if(order == null){
        prepareSql = 'SELECT * FROM '+table+'';
    }else{
        prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
    }
    db.query(prepareSql,function (err, rows) {
        if (err) throw err;
        cb(rows)
    });
};


var checkUser = function (token) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT * FROM `places_users` WHERE `token` = '"+token+"'";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};

var getPlaceInfo = function (id,cb) {
    let prepareSql = '';
    prepareSql = "" +
        "SELECT `places_users`.`name`, " +
        "`places_lang`.`name`,`places_lang`.`address`,`places_lang`.`lang_id`,`places_lang`.`id` as places_lang_id, " +
        "`places`.`phone`,`places`.`email`,`places`.`id` as place_id " +
        "FROM `places_users` " +
        "JOIN `places_lang` ON `places_lang`.`places_id` = `places_users`.`place_id` " +
        "JOIN `places` ON `places`.`id` = `places_users`.`place_id`" +
        "WHERE `places_users`.`id` = '"+db.escape(id)+"'";
    db.query(prepareSql,function (err, rows) {
        if (err) throw err;
        cb(rows)
    });
}

var getUserPermissions = function (id) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT `places_users_permissions`.`id`,`places_users_permissions`.`permissions_id`," +
            "`places_users_permissions`.`is_open`,`places_users_permissions`.`read_or_write`" +
            "FROM `places_users_permissions` " +
            "WHERE `places_users_permissions`.`places_users_id` = '"+db.escape(id)+"'";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};

var getPlaceEvents = function (placesId,lang_id) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT `events`.`id`, `events_lang`.`name` " +
            " FROM `events` " +
            " JOIN `events_lang` ON `events_lang`.`events_id` = `events`.`id` AND `events_lang`.`lang_id` = "+db.escape(lang_id)+
            " WHERE `events`.`places_id` = '"+db.escape(placesId)+"'";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};

var getThisEvents = function (eventsId) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT " +
            " `events`.`id`, `events`.`main_img`, `events`.`video_url`, `events`.`runtime`, `events`.`event_language`," +
            " GROUP_CONCAT(DISTINCT `events_slide`.`img` SEPARATOR '||') AS events_slide "+
            " FROM `events` " +
            " JOIN `events_slide` ON `events_slide`.`events_id` = "+db.escape(eventsId)+
            " WHERE `events`.`id` = "+db.escape(eventsId)+"";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};


var getThisEventMap = function (eventsId) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT  `event_dates`.`id` , " +
            " `event_dates`.`date`,`event_dates`.`time`," +
            " GROUP_CONCAT(DISTINCT `events_prices`.`id` SEPARATOR '||') AS events_prices_id, "+
            " GROUP_CONCAT(DISTINCT `events_prices`.`price` SEPARATOR '||') AS price, "+
            " GROUP_CONCAT(DISTINCT `events_prices`.`map_nstatex_group_price_id` SEPARATOR '||') AS map_nstatex_group_price_id "+
            " FROM `events_prices` " +
            " JOIN `event_dates` ON `event_dates`.`id` = `events_prices`.`event_dates_id`"+
            " WHERE `event_dates`.`events_id` = "+db.escape(eventsId)+" GROUP BY `event_dates`.`date`,`event_dates`.`time`";
        // console.log('prepareSql',prepareSql);
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};

var checkUserByPasswordAndToken = function (password,token) {
    return new Promise(function (resolve, reject) {
        let prepareSql = "SELECT `id` FROM `admin_clients` WHERE `pass` = '"+password+"' AND `token` = '"+token+"'";
        db.query(prepareSql, function (error, rows) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
};

/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAllSortByOrd = function(table,type_id,order,cb){
    let prepareSql = '';

    let orderString = '';
    if(order == null){
        orderString = '';
    }else{
        orderString = 'ORDER BY ord '+ order +'';
    }
    prepareSql = 'SELECT '+table+'.* , `show_info`.`post_id`, `show_info`.`type_id` , `show_info`.`id` as sid ' +
        ' FROM `'+table+'` ' +
        ' LEFT JOIN `show_info` ON `show_info`.`post_id` = `'+ table+'`.`id` AND `show_info`.`type_id` = '+type_id+' '+
        ' '+orderString;
    db.query(prepareSql,function (err, rows, fields) {
        if (err == null) {
            cb(rows)
        }else{
            cb('9999')
        }

    });
};

var findById = function(table,id,cb) {
    var prepareSql = "SELECT * FROM " + table + " WHERE `id`=" + db.escape(id);
    db.query(prepareSql, function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    })
}

var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            // console.log('error',error)
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}


/**
 *
 * @param table
 * @param post {
 *              values: {fildname:value,.....}
 *              where: {fildname:value,.....}
 *          }
 * @param cb
 */

var update = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    // console.log('update',prepareSql);
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }

    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    //console.log('update',prepareSql);
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var update2vPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post.values) {
            val.push("`" + key + "`=" + db.escape(post.values[key]));
        }
        var whereVal = [];
        for (var key in post.where) {
            whereVal.push("`" + key + "`" + "=" + db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";

        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "update2vPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName2v = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

var findByMultyNamePromise = function (table,params,filds) {
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for (var key in params) {
            if (params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            } else {
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE ' + whereParams.join(' AND ') + '';

        // console.log('prepareSql', prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        });
    });
}
var findUserPermissions = function (userId) {
    return new Promise(function (resolve, reject) {

        var prepareSql = 'SELECT `places_users_permissions`.`permissions_id`,`places_users_permissions`.`places_users_id` as user_id,' +
            ' `places_users_permissions`.`is_open`,`places_users_permissions`.`read_or_write`,' +
            ' `permissions`.`name`,`permissions`.`display_name`' +
            ' FROM `places_users_permissions`' +
            ' JOIN `permissions` ON `permissions`.`id` = `places_users_permissions`.`permissions_id`' +
            ' WHERE `places_users_permissions`.`places_users_id` = '+db.escape(userId)+'';
        // console.log('prepareSql', prepareSql);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        });
    });
};

var findByUrl = function (table,url,filds,cb) {

    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE `url_path` LIKE "'+url+'%"';
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}


/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    //console.log(prepareSql);
    db.query(prepareSql,post,function(error,result,fields){
        if (error) throw error;
        cb(result)
    });
}

var findAllPromise = function(table,order){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';
        if(order == null){
            prepareSql = 'SELECT * FROM '+table+'';
        }else{
            prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
        }
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

// var getPlaceMaps = function(place_id){
//     return new Promise(function (resolve, reject) {
//         var prepareSql = '' +
//             'SELECT `map`.`name` as map_name,`map_nstatex_group_price`.`name` as group_name,`map_nstatex_group_price`.`color` as group_color ' +
//             'FROM `map` ' +
//             'JOIN `map_nstatex_group_price` ON `map_nstatex_group_price`.`map_id` = `map`.`id` ' +
//             'WHERE `map`.`places_users_id` ='+db.escape(place_id)+''
//         ;
//
//         db.query(prepareSql, function (error, rows, fields) {
//             if (error){
//                 reject(error)
//             }else{
//                 resolve(rows)
//             }
//         })
//     })
// }

var findAllByLangPromise = function(table,order,lang_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';
        if(order == null){
            prepareSql = 'SELECT * FROM '+table+' WHERE `lang_id` = '+db.escape(lang_id)+'';
        }else{
            prepareSql = 'SELECT * FROM '+table+' WHERE `lang_id` = '+db.escape(lang_id)+' ORDER BY id '+ order +'';
        }

        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}

var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                };
                reject(error)
            }else{
                resolve(results)
            }
        });
    })
}

function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/map.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}


module.exports.adminLogin = adminLogin;
module.exports.findAll = findAll;
// module.exports.getPlaceMaps = getPlaceMaps;
module.exports.findAllPromise = findAllPromise;
module.exports.findAllByLangPromise = findAllByLangPromise;
module.exports.findById = findById;
module.exports.insert2v = insert2v;
module.exports.update = update;
module.exports.update2v = update2v;
module.exports.findByMultyName2v = findByMultyName2v;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.findByUrl = findByUrl;
module.exports.deletes = deletes;
module.exports.findAllSortByOrd = findAllSortByOrd;
module.exports.checkUser = checkUser;
module.exports.getUserPermissions = getUserPermissions;
module.exports.getPlaceEvents = getPlaceEvents;
module.exports.getThisEvents = getThisEvents;
module.exports.getThisEventMap = getThisEventMap;
module.exports.checkUserByPasswordAndToken = checkUserByPasswordAndToken;
module.exports.getPlaceInfo = getPlaceInfo;
module.exports.update2vPromise = update2vPromise;
module.exports.findUserPermissions = findUserPermissions;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.insert2vPromise = insert2vPromise;
