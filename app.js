var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const request = require('request');
var statics = require('./static');

var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");

var mids = require('./middleware')


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var footballMatchRouter = require('./routes/football/match');
var footballCountryRouter = require('./routes/football/country');
var footballLeagueRouter = require('./routes/football/league');
var footballTeamRouter = require('./routes/football/team');
var footballPlayerRouter = require('./routes/football/player');
var authRouter = require('./routes/auth/auth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



app.enable('trust proxy')
app.use(function (req, res, next) {
  // var ip = req.connection.remoteAddress;
  // console.log('ip',ip)
  res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization");;
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
  res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
  res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
  res.removeHeader('Server');
  res.removeHeader('X-Powered-By');
  next()
})

app.use(function (req, res, next) {mids.checkUser(req, res, next)});

app.use(function (req, res, next) {mids.getCountries(req, res, next)});
app.use(function (req, res, next) {mids.setlanguage(req, res, next)});
// app.use(function (req, res, next) {mids.getTopLeagues(req, res, next)});


app.use(function (req, res, next) {mids.getTimeInfo(req, res, next)});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/football/match', footballMatchRouter);
app.use('/football/country', footballCountryRouter);
app.use('/football/league', footballLeagueRouter);
app.use('/football/team', footballTeamRouter);
app.use('/football/player', footballPlayerRouter);
app.use('/auth', authRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
