var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../static');
var language = require('../language');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        userData : req.userInfo,
        lang : req.lang,
        language : language.langs,
        likeLeagues : req.likeLeagues,
        likeTeams : req.likeTeams,
        countries : req.countries
    });
});

router.post('/getGamesPerDay', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/livescores/for_spec_day',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            page : typeof req.body.page === "undefined" ? 1 : req.body.page,
            timezone : req.timezone,
            day:req.body.day
        }
    };
    // console.log('XXXXXXXXXXXXx',options)
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data : result.data})
            }
        }
    });

});

router.post('/getLivescores', function(req, res, next) {

    const options = {
        url: ''+statics.API_URL+'/football/livescores/',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            page : typeof req.body.page === "undefined" ? 1 : req.body.page,
            timezone : req.timezone
        }
    };
    // console.log('XXXXXXXXXXXXx',options)
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data : result.data})
            }
        }
    });

});

router.post('/getLivescoresNow', function(req, res, next) {

    const options = {
        url: ''+statics.API_URL+'/football/livescores/now',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            timezone : req.timezone
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                //console.log('result.error',result)
                res.json({error:false,data : result.data})
            }
        }
    });

});

module.exports = router;
