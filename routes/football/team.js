var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../../static');
var language = require('../../language');



router.get('/:id', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/teams/def-info',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            teamId : req.params.id
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.render('football/team/team', {
                    userData : req.userInfo,
                    countries : req.countries,
                    likeLeagues : req.likeLeagues,
                    likeTeams : req.likeTeams,
                    lang : req.lang,
                    language : language.langs,
                    data:result.data
                });
            }
        }
    });
});


router.post('/item', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/teams/item',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            teamId : req.body.teamId,
            timezone : req.timezone
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });

});


router.post('/fixtures', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/teams/fixtures',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            teamId : req.body.teamId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });

});

module.exports = router;
