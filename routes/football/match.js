var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../../static')
var language = require('../../language');


/* GET home page. */
router.get('/:id', function(req, res, next) {
    // console.log(req.params.id)
    res.render('football/match',{
        userData : req.userInfo,
        lang : req.lang,
        matchId : req.params.id,
        language : language.langs,
    });
});

router.post('/get-match', function(req, res, next) {
    // console.log('req.body.matchId',req.body.matchId)
    const options = {
        url: ''+statics.API_URL+'/football/match/get-match',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            matchId : req.body.matchId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // console.log('...result',result)
            if(!result.error){
                res.json({error:false,data : result.data})
            }
        }
    });

});

router.post('/get-h2h', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/match/get-h2h',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            timezone : req.timezone,
            team1id : req.body.localTeamId,
            team2id : req.body.visitorTeamId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data : result.data})
            }
        }
    });

});

module.exports = router;
