var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../../static');
var language = require('../../language');



router.get('/standing/:id', function(req, res, next) {
    //console.log('language.langs',language.langs);
    res.render('football/league/p_standings', {
        lang : req.lang,
        league_id:req.params.id,
        language : language.langs,
    });
});
router.post('/standing', function (req,res,next) {
    const options = {
        url: ''+statics.API_URL+'/football/leagues/stand-for-popup',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            league_id : req.body.league_id
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // console.log('result',result)
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });
})
router.post('/topscorers', function (req,res,next) {
    //console.log(' req.body.league_id', req.body.league_id)
    const options = {
        url: ''+statics.API_URL+'/football/leagues/topscorers',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            league_id : req.body.league_id
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // console.log('result',result)
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });
})
router.get('/:id', function(req, res, next) {
    //console.log('league req.params.id',req.params.id);
    const options = {
        url: ''+statics.API_URL+'/football/leagues/def-info',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            league_id : req.params.id
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                //console.log('result',result.data)
                res.render('football/league/league', {
                    userData : req.userInfo,
                    countries : req.countries,
                    likeLeagues : req.likeLeagues,
                    likeTeams : req.likeTeams,
                    lang : req.lang,
                    language : language.langs,
                    data:result.data
                });
            }
        }
    });
});


router.post('/standings', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/leagues/standings',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            seasonId : req.body.seasonId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });

});

router.post('/add-like', function(req, res, next) {
    var type,token;
    if(typeof req.cookies.fin !== "undefined"){
        type = 1;
        token = req.cookies.fin
    }else{
        type = 0;
        token = req.cookies.gin
    }
    const options = {
        url: ''+statics.API_URL+'/football/leagues/add-like',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            leagueId : req.body.leagueId,
            type :type,
            token:token
        }
    };
    // request(options, function (error, response, body) {
    //     if (!error && response.statusCode == 200) {
    //         const result = JSON.parse(body);
    //         if(!result.error){
    //             res.json({error:false})
    //         }else {
    //             res.json({error:true})
    //         }
    //     }
    // });

});
router.post('/del-like', function(req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/football/leagues/del-like',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            leagueId : req.body.leagueId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false})
            }else {
                res.json({error:true})
            }
        }
    });

});

router.post('/results', function(req, res, next) {
    var timezone = req.timezone;
    const options = {
        url: ''+statics.API_URL+'/football/leagues/results',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            leagueId : req.body.leagueId,
            seasonId : req.body.seasonId,
            timezone:timezone
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            //console.log('result',result)
            if(!result.error){
                res.json({error:false,data:result.data})
            }
        }
    });

});


module.exports = router;
