var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../../static');
var language = require('../../language');


router.get('/:id', function(req, res, next) {
    //console.log('country req.params.id',req.params.id)
    const options = {
        url: ''+statics.API_URL+'/football/countries/item',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            country_id : req.params.id,
            timezone : req.timezone
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.render('football/country', {
                    userData : req.userInfo,
                    countries : req.countries,
                    likeLeagues : req.likeLeagues,
                    likeTeams : req.likeTeams,
                    lang : req.lang,
                    language : language.langs,
                    data:result.data
                });
            }
        }
    });

});

// router.post('/getLivescores', function(req, res, next) {
//

//
// });

module.exports = router;
