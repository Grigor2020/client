var express = require('express');
var router = express.Router();
var request = require('request');
var statics = require('../../static')
var language = require('../../language');


/* GET home page. */
router.get('/:id', function(req, res, next) {
    // console.log(req.params.id)
    res.render('football/player/player',{
        userData : req.userInfo,
        countries : req.countries,
        likeLeagues : req.likeLeagues,
        likeTeams : req.likeTeams,
        lang : req.lang,
        language : language.langs,
        playerId : req.params.id,
    });
});

router.post('/get-player', function(req, res, next) {
    //console.log('req.body.playerId',req.body.playerId)
    const options = {
        url: ''+statics.API_URL+'/football/player/get-players',
        method : "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            playerId : req.body.playerId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // console.log('...result',result)
            if(!result.error){
                res.json({error:false,data : result.data})
            }
        }
    });

});

module.exports = router;
