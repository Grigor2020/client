var express = require('express');
var router = express.Router();
const request = require('request');
var static = require('../../static');

/* GET users listing. */
router.post('/sign-up', function(req, res, next) {
    if(req.body.email.length > 0 && req.body.password.length > 0){
        const formData = {
            "email" :req.body.email,
            "password" :  req.body.password
        };
        request.post(
            {
                url: static.API_URL+'/auth/sign-up',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.cookie('fin', jsonBody.user.token);
                    res.redirect('/');
                }else{
                    res.json({error:true, errorCode : jsonBody.msg})
                }
                res.end();
            }
        )
    }
});

router.post('/login', function(req, res, next) {
    if(req.body.email.length > 0 && req.body.password.length > 0){
        const formData = {
            "email" :req.body.email,
            "password" :  req.body.password
        };
        request.post(
            {
                url: static.API_URL+'/auth/login',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    // console.log('jsonBody',jsonBody)
                    res.cookie('fin', jsonBody.user.token);
                    res.redirect('/');
                    res.end();
                }else{
                    // res.json({error:true, errorCode : jsonBody.msg})
                    res.redirect('/');
                    res.end();
                }
                res.end();
            }
        )
    }
});

router.get('/logout', function(req, res, next) {
    res.clearCookie('fin');
    res.redirect('/');
    res.end();

});

module.exports = router;
